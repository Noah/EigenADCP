#pragma once

/**
 * @brief Enum defining the IDs for the buttons used in the application.
 * @details These IDs are used for handling events from the corresponding buttons.
 * Each ID should be unique within the context where it is being used. These IDs
 * are following the wxWidgets guideline of starting custom IDs at `wxID_HIGHEST + 1`.
 */
enum {

	START_BUTTON = wxID_HIGHEST + 1,
	COPY_DECLARATIONS_BUTTON,
	COPY_ADJOINTS_BUTTON,
	COPY_TANGENT_BUTTON,

};
