#pragma once
#include <wx/app.h>
/**
 * @class EigenADApp
 * @brief Main application class.
 * @details This class is responsible for initializing the main frame of the application.
 * It is derived from wxApp, and is used to set up the top-level GUI object.
 */
class EigenADApp : public wxApp {
public:
    /**
     * @brief Initialization function.
     * @details Called when the application is first started. It's used to initialize the application,
     * setting up the main frame and other relevant initialization tasks.
     * @return Returns true if initialization was successful, false otherwise.
     */
    virtual bool OnInit();
};