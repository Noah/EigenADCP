#pragma once

#include <wx/stattext.h>
#include <wx/panel.h>
#include <wx/button.h>

/**
 * @class AdjointsPanel
 * @brief Class for panel displaying adjoints and providing a copy functionality.
 * @details This class extends the wxPanel class and provides a UI panel which displays adjoints and has a button to copy them.
 */
class AdjointsPanel : public wxPanel {
public:
	/**
	 * @brief Constructor.
	 * @param parent The parent window of this panel.
	 */
	AdjointsPanel(wxWindow* parent);

private:
	/**
	 * @brief The static text field showing adjoints' description.
	 */
	wxStaticText* adjDescription_;

	/**
	 * @brief The button to copy adjoints.
	 */
	wxButton* copyAdjButton_;
};

