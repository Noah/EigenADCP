#pragma once

#include <wx/panel.h>
#include <wx/stattext.h>
#include <wx/button.h>

/**
 * @class TangentPanel
 * @brief Class for panel displaying tangents and providing a copy functionality.
 * @details This class extends the wxPanel class and provides a UI panel which displays tangents and has a button to copy them.
 */
class TangentPanel : public wxPanel {
public:
	/**
	 * @brief Constructor.
	 * @param parent The parent window of this panel.
	 */
	TangentPanel(wxWindow* parent);

private:
	/**
	 * @brief The static text field showing the tangents' description.
	 */
	wxStaticText* tangDescription_;

	/**
	 * @brief The button to copy tangents.
	 */
	wxButton* copyTangButton_;
};

