#pragma once

#include <wx/panel.h>
#include <wx/spinctrl.h>
#include <wx/button.h>
#include <wx/checkbox.h>

/**
 * @class ConditionControlPanel
 * @brief Class for panel containing input box for derivative order and start button.
 * @details This class extends the wxPanel class and provides a UI panel which contains a spin control for inputting derivative order and a start button.
 */
class ConditionControlPanel : public wxPanel {
public:
    /**
     * @brief Constructor.
     * @param parent The parent window of this panel.
     */
    ConditionControlPanel(wxWindow* parent);

    /**
     * @brief Gets the derivative order entered by the user.
     * @return The derivative order entered.
     */
    int getDerivativeOrder() const;
    /**
     * @brief Gets the value of the checkbox for enabling the optimized output.
     * @return True or False depending if the checkbox is ticked.
     */
    bool getOptimizedOutputCheckBoxValue() const;
    /**
    * @brief Gets the value of the checkbox for enabling the duality validation.
    * @return True or False depending if the checkbox is ticked.
    */
    bool getDualityValidationCheckBoxValue() const;
    /**
    * @brief Gets the value of the checkbox for enabling the Forward FDA.
    * @return True or False depending if the checkbox is ticked.
    */
    bool getForwardFDACheckBoxValue() const;
    /**
    * @brief Gets the value of the checkbox for enabling the Backward FDA.
    * @return True or False depending if the checkbox is ticked.
    */
    bool getBackwardFDACheckBoxValue() const;
    /**
    * @brief Gets the value of the checkbox for enabling the Central FDA.
    * @return True or False depending if the checkbox is ticked.
    */
    bool getCentralFDACheckBoxValue() const;
private:
    /**
     * @brief The spin control for entering derivative order.
     */
    wxSpinCtrl* inputDerivBox_;

    /**
     * @brief The start button to initiate the main functionalities of this tool.
     */
    wxButton* startButton_;

    /**
     * @brief The checkbox to enable an optimized output.
     * @details If enabled each .llt() operator is replaced with a string.
     */
    wxCheckBox* enableOptimizedOutput_;

    /**
     * @brief The checkbox to enable duality validation after differentiating.
     */
    wxCheckBox* enableDualityValidation_;
    /**
     * @brief The checkbox to enable a forward finite differences approximation after differentiating.
     */
    wxCheckBox* enableForwardFDAValidation_;
    /**
     * @brief The checkbox to enable a backward finite differences approximation after differentiating.
     */
    wxCheckBox* enableBackwardFDAValidation_;
    /**
     * @brief The checkbox to enable a central finite differences approximation after differentiating.
     */
    wxCheckBox* enableCentralFDAValidation_;
    
};
