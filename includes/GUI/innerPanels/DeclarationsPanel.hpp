#pragma once

#include <wx/panel.h>
#include <wx/button.h>
#include <wx/stattext.h>


/**
 * @class DeclarationsPanel
 * @brief Class for panel displaying matrix declarations and providing a copy functionality.
 * @details This class extends the wxPanel class and provides a UI panel which displays matrix declarations and has a button to copy them.
 */
class DeclarationsPanel : public wxPanel {
public:
	/**
	 * @brief Constructor.
	 * @param parent The parent window of this panel.
	 */
	DeclarationsPanel(wxWindow* parent);

private:
	/**
	 * @brief The static text field showing the declarations' description.
	 */
	wxStaticText* declDescription_;

	/**
	 * @brief The button to copy declarations.
	 */
	wxButton* copyDeclButton_;
};

