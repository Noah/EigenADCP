#pragma once
#include <string>

/**
 * @class Dimension
 * @brief Class representing a dimension (a row or a column) of matrices.
 * @details Each ExpMatrix points to two Dimension objects which are not necessarily different from each other after computing dimensions.
 */
class Dimension {
public:
	/**
	 * @brief Default constructor for the Dimension class.
	 * @details Only used to initialize the pointers in the ExpMatrix class.
	 */
	Dimension();
	/**
	 * @brief Constructor for the Dimension class.
	 * @param size The size of the dimension.
	 */
	Dimension(int size);
	/**
	 * @brief Constructor for the Dimension class.
	 * @param size The size of the dimension.
	 * @param outputNum The output numeration of the dimension.
	 */
	Dimension(int size, int outputNum);
	/**
	 * @brief Get a string representation of the Dimension.
	 * @return A string representation of the Dimension.
	 */
	std::string toString() const;
	/**
	 * @brief Get the size of the Dimension.
	 * @return The size of the Dimension.
	 */
	int getSize() const;

	/**
	 * @brief Set the size of the Dimension.
	 * @param size The size of the Dimension.
	 */
	void setSize(int size);

	/**
	 * @brief Set the output numeration of the Dimension.
	 * @param num The output numeration of the Dimension.
	 */
	void setNum(int num);

	/**
	 * @brief Get the output numeration of the Dimension.
	 * @return The output numeration of the Dimension.
	 */
	int getNum() const;
private:
	/**
	 * @brief The size of the dimension.
	 */
	int size_ = -1;
	/**
	 * @brief The output numeration of the dimension.
	 */
	int outputNumeration_ = -1;
	/**
	 * @brief The ID of the dimension (for debugging purposes).
	 */
	int id_ = -1; 
	/**
	 * @brief The numerator (for debugging purposes).
	 */
	static int numerator; 

};
