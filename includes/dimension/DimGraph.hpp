#pragma once
#include <vector>
#include <memory>

#include "DimensionType.hpp"

#include "DimNode.hpp"

#include "Dimension.hpp"

/**
 * @class DimGraph
 * @brief Class representing a dimension graph.
 * @details This class is used to hold all DimNodes and connect them such that finally all DimNodes that each represent a Dimension of a matrix and need to be the same in order for the original expression to be valid are connected.
 */
class DimGraph {
public:
	/**
	 * @brief Default constructor for DimGraph.
	 */
	DimGraph();
	/**
	 * @brief Connects a node to another node with the specified hash ID and dimension type,
	 *        and adds the connected node if it does not already exist.
	 * @param node The node to connect (and possibly add).
	 * @param toHashID The hash ID of the node to connect to.
	 * @param toDimType The dimension type of the node to connect to.
	 */
	void connectToAndAddIfAbsent(std::shared_ptr<DimNode> node, const std::string& toHashID,
		DimensionType toDimType);
	/**
	 * @brief Adds two nodes if they do not already exist and connects them.
	 * @param first The first node.
	 * @param second The second node.
	 */
	void addIfAbsentAndConnect(std::shared_ptr<DimNode> first, std::shared_ptr<DimNode> second);
	
	/**
	 * @brief Generates matrix dimensions based on the connections in the graph.
	 */
	void generateMatrixDimensions();
	/**
	 * @brief Converts the dimension graph to a string representation.
	 * @return The string representation of the dimension graph.
	 */
	std::string toString() const;
	/**
	 * @brief Retrieves the dimension declarations as a string.
	 * @return The dimension declarations as a string.
	 */
	std::string getDimensionDeclarations() const;
private:
	/**
	 * @brief Helper function to add a single node to the graph if it does not already exist.
	 * @param node The node to add.
	 */

	void addIfAbsent(std::shared_ptr<DimNode> node);
	/**
	 * @brief Connects a node to its equivalent node in the graph.
	 * @details This also includes nodes that represent a dimension of a derivative of a matrix.
	 * @param node The node to connect.
	 */
	void connectToEquiv(std::shared_ptr<DimNode> node);
	/**
	 * @brief All nodes in the graph.
	 * @details This is later used to generate all dimension values.
	 */
	std::vector<std::shared_ptr<DimNode>> allNodes_;
	/**
	 * @brief All dimensions in the graph.
	 * @details This is later used to output all dimension declarations.
	 */
	std::vector<Dimension> allDimensions_;
};