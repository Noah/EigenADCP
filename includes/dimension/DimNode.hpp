#pragma once		
#include <string>
#include <vector>
#include <memory>
#include "DimensionType.hpp"

/**
 * @brief Standard value if a DimNode has no ExpMatrix object it belongs to and therefore no matrixName_.
 */
constexpr const char* NO_NAME = "no_name";

class Dimension;

/**
 * @class DimNode
 * @brief Class representing a node in the dimension graph.
 * @details A DimNode represents one of the two resulting dimensions (row/column) of an expression object if it yields in a matrix.
 */
class DimNode {
public:
	/**
	 * @brief Default constructor for DimNode.
	 */
	DimNode() = default;

	/**
	* @brief Constructor for DimNode with hash ID and dimension type.
	* @param hashID The hash ID of the Expression this node represents a dimension of.
	* @param dimType The dimension type this node shall represent.
	*/
	DimNode(const std::string& hashID, DimensionType dimType);

	/**
	 * @brief Converts the node to a string representation.
	 * @return The string representation of the node.
	 */
	std::string toString() const;
	/**
	 * @brief Retrieves the dimension type of the node.
	 * @return The dimension type of the node.
	 */
	DimensionType getDimType() const;
	/**
	 * @brief Checks if the node has been discovered in a graph traversal.
	 * @return True if the node has been discovered, false otherwise.
	 */
	bool isDiscovered() const;
	/**
	 * @brief Sets the node as discovered in a graph traversal.
	 */
	void setDiscovered();

	/**
	 * @brief Retrieves the hash ID of the Expression this node represents a dimension of.
	 * @return The hash ID of the Expression this node represents a dimension of.
	 */
	std::string getHashID() const;

	/**
	 * @brief Connects two nodes.
	 * @details Adds an entry for each node in connectedNodes_.
	 * @param node1 The first node to connect.
	 * @param node2 The second node to connect.
	 */
	static void connect(const std::shared_ptr<DimNode> node1, const std::shared_ptr<DimNode> node2);

	/**
	 * @brief Sets a dimension of the connected nodes and traverses each connected node again until all connections are traversed.
	 * @param dim The dimension to set.
	 */
	void setDimOfConnected(const Dimension& dim);

	/**
	 * @brief Checks if the node is identical to another node by comparing the hashID_ and the dimType_.
	 * @param node The node to compare with.
	 * @return True if the nodes are identical, false otherwise.
	 */
	bool isIdenticalTo(std::shared_ptr<DimNode> node);
	/**
	 * @brief Checks if the node is connected to another node.
	 * @param node The node to check.
	 * @return True if the nodes are connected, false otherwise.
	 */
	bool isConnectedTo(const std::shared_ptr<DimNode>& node) const;

	/**
	 * @brief Retrieves the name of the node.
	 * @return The name of the node.
	 */
	std::string getMatrixName() const;
protected:
	/**
	 * @brief The name of the matrix this node belongs to.
	 * @details Only contains a meaningful value if the DimNode is of type DimNodeMatrix.
	 */
	std::string matrixName_ = NO_NAME;
	/**
	 * @brief The hash ID of the expression object this node represents a dimension of.
	 */
	std::string hashID_;
	/**
	 * @brief The dimension type this node represents.
	 */
	DimensionType dimType_ = DimensionType::Unset;
	/**
	 * @brief Flag indicating if the node has been discovered in a graph traversal.
	 */
	bool discovered_ = false;
	/**
	 * @brief List of connected nodes.
	 */
	std::vector<std::weak_ptr<DimNode>> connectedNodes_;
	/**
	 * @brief Helper function that sets the dimension of the node.
	 * @details A dimension is only set if the DimNode is of type DimNodeMatrix.
	 * @param dim The dimension to set.
	 */
	virtual void setDimension(const Dimension& dim);
};