#pragma once
#include "DimNode.hpp"

#include <string>
#include <memory>

#include "Dimension.hpp"
#include "DimensionType.hpp"

class Dimension;

/**
 * @class DimNodeCholesk
 * @brief Class representing a cholesky node node in the dimension graph. This class is a subclass of DimNode.
 * @details A DimNodeCholesk represents one of the two resulting dimensions (row/column) of a ExpCholesk object.
 */
class DimNodeCholesk : public DimNode {
public:
    /**
     * @brief Constructor for DimNodeCholesk.
     * @param hashID The hash ID of the ExpCholesk node this node represents a dimension of.
     * @param dimType The dimension type of ExpCholesk node this node represents a dimension of.
     * @param dim The shared pointer to the dimension object it refers to.
     */
    DimNodeCholesk(const std::string& hashID, DimensionType dimType, std::shared_ptr<Dimension> dim);
    /**
     * @brief Overridden helper function that sets the dimension of the node.
     * @details A dimension is only set if the DimNode is of type DimNodeCholesk because a DimNode does not have a dimension pointer.
     * @param dim The dimension to set.
     */
    void setDimension(const Dimension& dim) override;
protected:
    /**
     * @brief The shared pointer to the dimension object it refers to.
     * @details This shared pointer points to one of the Dimension objects in a ExpCholesk object and is later used to set the size of the Dimension object of the said ExpCholesk object.
     */
    std::shared_ptr<Dimension> dim_;
};