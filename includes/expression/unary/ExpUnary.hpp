#pragma once
#include "expression/Expression.hpp"
#include "expression/DerivativeType.hpp"
#include <string>
#include <memory>
#include <vector>

#include "expression/ExpressionResultType.hpp"
#include "expression/DerivativeType.hpp"

class ExpMatrix;
class ExpAssign;
class MatrixCache;

/**
 * @class ExpUnary
 * @brief Abstract super class for unary expressions. This class is derived from the Expression base class.
 */
class ExpUnary : public Expression {
public:
    /**
    * @brief Constructs a unary expression with the given subexpression.
    * @details This constructor is used when first creating the tree and all other member variables are unknown.
    * @param exp Shared pointer to the subexpression.
    */
    ExpUnary(std::shared_ptr<Expression> exp);
    /**
    * @brief Constructs a unary expression with all given member variables.
    * @details This constructor is used when a deepCopy is needed.
    * @param exp Shared pointer to the subexpression.
    * @param resultType The result type of the assignment expression.
    */
    ExpUnary(std::shared_ptr<Expression> exp, ExpressionResultType resultType);

    virtual std::string toString() const = 0;
    virtual std::shared_ptr<Expression> tangDifferentiate() const = 0;
    virtual void adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const = 0;
    std::shared_ptr<Expression> deepCopy() const = 0;
    virtual ExpressionResultType checkSemantics() = 0;
    virtual void setDimensionNodes(DimGraph& graph) = 0;
    virtual std::variant<ScalarValueType, MatrixType> calculate() const = 0;
    void generateMatrixValues(MatrixCache& matrixValuesCache) override;
    virtual std::vector<std::vector<ExpressionResultType>> getRequiredTypes() const = 0;
    virtual ExpressionType getExpressionType() const = 0;
    void perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) override;
    void devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache);
    virtual std::shared_ptr<Expression> transformExpCholeskToExpLowUpp() const = 0;
protected:
    /**
     * @brief A shared pointer to the subexpression.
     */
    std::shared_ptr<Expression> exp_;

};