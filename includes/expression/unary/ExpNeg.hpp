#pragma once
#include "ExpUnary.hpp"

#include <memory>
#include <string>
#include <vector>

#include "expression/ExpressionResultType.hpp"

class Expression;
class DimGraph;
class ExpMatrix;
class ExpAssign;
class MatrixCache;

/**
 * @class ExpNeg
 * @brief This class represents a negation operation. This class is derived from the Expression base class.
 */
class ExpNeg : public ExpUnary {
public:
    using ExpUnary::ExpUnary;
    std::string toString() const override;
    std::shared_ptr<Expression> tangDifferentiate() const override;
    void adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const override;
    void setDimensionNodes(DimGraph& graph) override;
    std::variant<ScalarValueType, MatrixType> calculate() const override;
    std::shared_ptr<Expression> deepCopy() const override;
    ExpressionResultType checkSemantics() override;
    std::vector<std::vector<ExpressionResultType>> getRequiredTypes() const override;
    ExpressionType getExpressionType() const override;
    std::shared_ptr<Expression> transformExpCholeskToExpLowUpp() const override;
private:
};
