#pragma once

/**
 * @enum ExpressionType.
 * @brief The type of the Expression.
 * @details Here all non abstract subclasses of Expression must be listed.
 */
enum class ExpressionType {
	Matrix,
	Scalar,
	
	Negation,
	Parenthesis,
	Transposition,

	Addition,
	Subtraction,
	Multiplication,
	Cholesky_Factorization,
	Lower_Upper_Factorization,

	Assignment,
};

/**
 * @brief Convert an ExpressionType value to its corresponding string representation.
 * @param type The ExpressionType value to convert.
 * @return The string representation of the given ExpressionType value.
 */
static std::string ExpressionTypeToString(ExpressionType type) {
	switch (type) {
	case ExpressionType::Matrix:
		return "Matrix";
	case ExpressionType::Scalar:
		return "Scalar";

	case ExpressionType::Negation:
		return "Negation";
	case ExpressionType::Parenthesis:
		return "Parenthesis";
	case ExpressionType::Transposition:
		return "Transposition";

	case ExpressionType::Addition:
		return "Addition";
	case ExpressionType::Subtraction:
		return "Subtraction";
	case ExpressionType::Multiplication:
		return "Multiplication";
	case ExpressionType::Cholesky_Factorization:
		return "Cholesky Factorization";
	case ExpressionType::Lower_Upper_Factorization:
		return "Lower-Upper Factorization";

	case ExpressionType::Assignment:
		return "Assignment";

	default:
		return "Abstract";
	}

}
