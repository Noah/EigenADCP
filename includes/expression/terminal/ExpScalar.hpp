#pragma once
#include "ExpTerminal.hpp"

#include <memory>
#include <string>
#include <vector>
#include <variant>

#include "expression/Expression.hpp"
#include "expression/ExpressionResultType.hpp"
#include "expression/DerivativeType.hpp"
#include "EigenNS.hpp"

class Expression;
class DimGraph;

class ExpAssign;
class MatrixCache;

/**
 * @class ExpScalar
 * @brief This class represents a scalar. This class is derived from the Expression base class.
 */
class ExpScalar : public ExpTerminal {
public:
    /**
    * @brief Constructs a scalar with the given type and value.
    * @details This constructor is used when first creating the tree and all other member variables are unknown.
    * @param value The value of this scalar.
    */
    ExpScalar(ScalarValueType value);
    /**
    * @brief Constructs a scalar with the given type and value.
    * @details This constructor is used when a deepCopy is needed.
    * @param resultType The result type of the assignment expression.
    * @param value The value of this scalar.
    */
    ExpScalar(ScalarValueType value, ExpressionResultType resultType);
    std::string toString() const override;
    std::shared_ptr<Expression> tangDifferentiate() const override;
    void adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const override;
    void setDimensionNodes(DimGraph& graph) override;
    
    std::variant<ScalarValueType, MatrixType> calculate() const override;
    std::shared_ptr<Expression> deepCopy() const override;
    ExpressionResultType checkSemantics() override;
    void generateMatrixValues(MatrixCache& matrixValuesCache) override;
    std::vector<std::vector<ExpressionResultType>> getRequiredTypes() const override;
    ExpressionType getExpressionType() const override;
    void perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) override;
private:
    /**
     * @brief The value of the scalar.
     */
    ScalarValueType scalarValue_;
};
