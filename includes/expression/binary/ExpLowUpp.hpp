#pragma once
#include "expression/binary/ExpBinary.hpp"

#include <memory>
#include <string>
#include <vector>
#include <variant>

#include "EigenNS.hpp"
#include "expression/ExpressionResultType.hpp"
#include "expression/DerivativeType.hpp"


class Dimension;
class Expression;
class DimGraph;
class ExpAssign;
class MatrixCache;

class ExpLowUpp : public ExpBinary {
public:
	using ExpBinary::ExpBinary;
	std::string toString() const override;
	std::shared_ptr<Expression> tangDifferentiate() const override;
	void adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const override;
	void setDimensionNodes(DimGraph& graph) override;
	std::variant<ScalarValueType, MatrixType> calculate() const override;
	std::shared_ptr<Expression> deepCopy() const override;
	ExpressionResultType checkSemantics() override;
	std::vector<std::vector<ExpressionResultType>> getRequiredTypes() const override;
	ExpressionType getExpressionType() const override;
	void perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) override;
	void devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache) override;
	std::shared_ptr<Expression> transformExpCholeskToExpLowUpp() const override;
private:

};
