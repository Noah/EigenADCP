#pragma once
#include "expression/binary/ExpBinary.hpp"

#include <memory>
#include <string>
#include <vector>
#include <variant>

#include "EigenNS.hpp"
#include "expression/ExpressionResultType.hpp"
#include "expression/DerivativeType.hpp"

class Dimension;
class Expression;
class DimGraph;
class ExpAssign;
class MatrixCache;

/**
 * @class ExpCholesk
 * @brief This class represents a cholesky factorization operation. This class is derived from the Expression base class.
 * @details This class includes the cholesky factorization and the solving of a linear system using the factorization.
 */
class ExpCholesk : public ExpBinary {
public:
	using ExpBinary::ExpBinary;
	/**
	 * @brief Constructs a cholesky factorization operation with all given member variables.
	 * @details This constructor is used when a deepCopy is needed.
	 * @param left A shared pointer to the left-hand side expression.
	 * @param right A shared pointer to the right-hand side expression.
	 * @param resultType The result type of the assignment expression.
	 * @param nameSubst The substitution string for the specific cholesky factorization.
	 * @param row A shared pointer to a Dimension object. 
    * @param column A shared pointer to a Dimension object.
    * @param lltValue The stored lltValue of this Cholesky Factorization.
	 */
	ExpCholesk(std::shared_ptr<Expression> left, std::shared_ptr<Expression> right, ExpressionResultType resultType, std::string nameSubst, std::shared_ptr<Dimension> row,
		std::shared_ptr<Dimension> column, Eigen::LLT<MatrixType> lltValue, MatrixType generatedLeftValues);
	std::string toString() const override;
	std::shared_ptr<Expression> tangDifferentiate() const override;
	void adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const override;
	void setDimensionNodes(DimGraph& graph) override;
	void generateMatrixValues(MatrixCache& matrixValuesCache) override;
	std::variant<ScalarValueType, MatrixType> calculate() const override;
	std::shared_ptr<Expression> deepCopy() const override;
	ExpressionResultType checkSemantics() override;
	std::vector<std::vector<ExpressionResultType>> getRequiredTypes() const override;
	ExpressionType getExpressionType() const override;
	void perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) override;
	/**
	 * @brief Create a row DimNode for this expression, overrides its inherited method.
	 * @details The resulting DimNodeMatrix is based upon the hash id and name of this ExpMatrix object.
	 * The DimNodeMatrix object contains a pointer to the row_ Dimension object of this matrix. This override is specific to ExpMatrix objects to incorporate these additional details.
	 * @returns A shared pointer to the created row DimNodeMatrix.
	 */
	std::shared_ptr<DimNode> createRowDimNode() const override;
	/**
	 * @brief Create a row DimNode for this expression, overrides its inherited method.
	 * @details The resulting DimNodeMatrix is based upon the hash id and name of this ExpMatrix object.
	 * The DimNodeMatrix object contains a pointer to the column_ Dimension object of this matrix. This override is specific to ExpMatrix objects to incorporate these additional details.
	 * @returns A shared pointer to the created column DimNodeMatrix.
	 */
	std::shared_ptr<DimNode> createColDimNode() const override;

	void devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache) override;

	std::shared_ptr<Expression> transformExpCholeskToExpLowUpp() const override;
private:
	/**
	 * @brief Debug toString() method that ignores the substitution of the factorization and outputs the original left subexpression.
	 * @returns A debug string.
	 */
	std::string toStringFull() const;
	/**
	 * @brief The current name of the substitution of the factoritation part used for the final output.
	 */
	std::string nameSubstitution_ = NO_CHOLESK_SUBST;
	/**
	 * @brief Value of the llt() operation that is randomly generated.
	 */
	Eigen::LLT<MatrixType> lltValue_;
	/**
	 * @brief The Values of the SPD matrix that is used to calculate the lltValue_.
	 */
	MatrixType generatedLeftValues_;
	/**
	 * @brief A shared pointer to a Dimension object that describes the row of the matrix.
	 * @details After calculating all Dimensions, a Dimension object does not need to be unique per matrix.
	 */
	std::shared_ptr<Dimension> row_ = std::make_shared<Dimension>();
	/**
	 * @brief A shared pointer to a Dimension object that describes the column of the matrix.
	 * @details After calculating all Dimensions, a Dimension object does not need to be unique per matrix.
	 */
	std::shared_ptr<Dimension> column_ = std::make_shared<Dimension>();
};