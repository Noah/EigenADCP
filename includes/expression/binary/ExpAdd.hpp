#pragma once
#include "ExpBinary.hpp"

#include <memory>
#include <string>
#include <vector>

#include "expression/ExpressionResultType.hpp"
#include "expression/DerivativeType.hpp"
class DimGraph;
class Expression;
class ExpAssign;
class MatrixCache;

/**
 * @class ExpAdd
 * @brief This class represents an addition operation. This class is derived from the Expression base class.
 * @details This class supports scalar/scalar, and matrix/matrix addition.
 */
class ExpAdd : public ExpBinary {
public:
    using ExpBinary::ExpBinary;
    std::string toString() const override;
    std::shared_ptr<Expression> tangDifferentiate() const override;
    void adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const override;
    void setDimensionNodes(DimGraph& graph) override;
    std::variant<ScalarValueType, MatrixType> calculate() const override;
    std::shared_ptr<Expression> deepCopy() const override;
    ExpressionResultType checkSemantics() override;
    std::vector<std::vector<ExpressionResultType>> getRequiredTypes() const override;
    ExpressionType getExpressionType() const override;
    void perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) override;
    std::shared_ptr<Expression> transformExpCholeskToExpLowUpp() const override;
private:

};
