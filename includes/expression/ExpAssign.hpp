#pragma once
#include <memory>
#include <string>
#include <vector>

#include "expression/Expression.hpp"
#include "expression/ExpressionResultType.hpp"

#include "EigenNS.hpp"

#include "dimension/Dimension.hpp"
#include "expression/DerivativeType.hpp"

class DimGraph;

class ExpMatrix;
class ExpAssign;
class MatrixCache;

/**
 * @class ExpAssign
 * @brief Class representing an assignment operation in an expression. This class is derived from the Expression on base class and serves as an entry point to many calculations done to the whole expression.
 */
class ExpAssign : public Expression {
public:
    /**
     * @brief Constructs an assignment expression with the given left and right expressions.
     * @details This constructor is used when first creating the tree and all other member variables are unknown.
     * @param left A shared pointer to the left-hand side matrix expression.
     * @param right A shared pointer to the right-hand side expression.
     */
    ExpAssign(std::shared_ptr<ExpMatrix> left, std::shared_ptr<Expression> right);
    /**
     * @brief Constructs an assignment expression with all given member variables.
     * @details This constructor is used when a deepCopy is needed.
     * @param left A shared pointer to the left-hand side matrix expression.
     * @param right A shared pointer to the right-hand side expression.
     * @param derType An enum value that determines what derivative this assignment describes.
     * @param resultType The result type of the assignment expression.
     */
    ExpAssign(std::shared_ptr<ExpMatrix> left, std::shared_ptr<Expression> right, DerivativeType derType,  ExpressionResultType resultType);
    std::string toString() const override;
    std::shared_ptr<Expression> tangDifferentiate() const override;
    void adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const override;
    void setDimensionNodes(DimGraph& graph) override;
    std::vector<std::vector<ExpressionResultType>> getRequiredTypes() const override;
    ExpressionType getExpressionType() const override;
    std::variant<ScalarValueType, MatrixType> calculate() const override;
    std::shared_ptr<Expression> deepCopy() const override;
    ExpressionResultType checkSemantics() override;
    void generateMatrixValues(MatrixCache& matrixValuesCache) override;
    void perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) override;
    /**
     * @brief Accumulates the adjoint for the current expression.
     * @details The additional adjoint is added onto the existing adjoint such that there is then single expression containing both.
     * @param adjoint The adjoint expression to accumulate.
     * @return A shared pointer to the new assignment expression with the accumulated adjoint.
     */
    std::shared_ptr<ExpAssign> accumulateAdjoint(const std::shared_ptr<Expression>& adjoint) const;
    /**
     * @brief Returns the left-hand side matrix expression.
     * @return The left-hand side matrix expression.
     */
    std::shared_ptr<ExpMatrix> getLeft() const;
    /**
     * @brief Returns the right-hand side subexpression.
     * @return The right-hand side subexpression.
     */
    std::shared_ptr<Expression> getRight() const;
    /**
     * @brief This function is the entry point to start differentiating at the top of an expression.
     * @throws EigenADException if a semantic or an internal error is detected.
     * @return The tangent derivative of the expression.
     */
    std::shared_ptr<ExpAssign> startTangDifferentiate() const;
    void devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache) override;

    /**
     * @brief Deep copies this ExpAssign object and returns a ExpAssign shared pointer.
     * @return A ExpAssign shared pointer to the copied expression.
     */
    std::shared_ptr<ExpAssign> deepCopyAssignment() const;
    std::shared_ptr<Expression> transformExpCholeskToExpLowUpp() const override;
    std::shared_ptr<ExpAssign> startTransformExpCholeskToExpLowUpp() const;
private:
    /**
     * @brief A shared pointer to the left-hand side ExpMatrix.
     */
    std::shared_ptr<ExpMatrix> left_;
    /**
     * @brief A shared pointer to the right-hand side Expression
     */
    std::shared_ptr<Expression> right_;

    /**
     * @brief Describes the derivative type this assignment represents.
     */
    DerivativeType derType_ = DerivativeType::Primal;
};
