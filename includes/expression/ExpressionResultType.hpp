#pragma once

/**
 * @enum ExpressionResultType
 * @brief The type of result that an expression can return.
 */
enum class ExpressionResultType {
    Matrix,
    Scalar,
	NotDetermined
};

/**
 * @brief Convert an ExpressionResultType value to its corresponding string representation.
 * @param type The ExpressionResultType value to convert.
 * @return The string representation of the given ExpressionResultType value.
 */
static std::string ExpressionResultTypeToString(ExpressionResultType type) {
	switch (type) {
	case ExpressionResultType::Matrix:
		return "Matrix";
	case ExpressionResultType::Scalar:
		return "Scalar";
	case ExpressionResultType::NotDetermined:
		return "Unset";
	default:
		return "none";
	}
}