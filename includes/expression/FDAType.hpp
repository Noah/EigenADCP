#pragma once

/**
 * @class FDAType
 * @brief Enumeration class to represent the types of Finite Difference Approximations.
 * @details Holds the different methods for approximating derivatives using finite differences.
 */
enum class FDAType {
    Forward,
    Backward,
    Central
};

/**
 * @brief Convert a FDAType to its string representation.
 * @param type The FDAType to convert.
 * @return The string representation of the FDAType.
 */
static std::string FDATypeToString(const FDAType& type) {
    switch (type) {
    case FDAType::Forward:
        return "Forward";
    case FDAType::Backward:
        return "Backward";
    case FDAType::Central:
        return "Central";
    default:
        return "Unknown";
    }
}