#pragma once
#include <string>
#include <memory>
#include <vector>
#include <variant>

#include "expression/ExpressionResultType.hpp"
#include "expression/ExpressionType.hpp"
#include "expression/DerivativeType.hpp"
#include "EigenNS.hpp"

class DimNode;
class ExpMatrix;
class DimGraph;
class ExpAssign;
class MatrixCache;

/**
 * @class Expression
 * @brief This class represents a mathematical expression.
 * @details This class is an abstract base class for all expressions.
 * It provides an interface for all derived classes to implement. 
 */
class Expression {
public:
    /**
     * @brief Return a string representation of the expression.
     * @returns The output of this expression as it would be in the final output.
     */
    virtual std::string toString() const = 0;
    /**
     * @brief Perform a recursive tangent differentiation on this expression.
     * @throws EigenADException if a semantic or an internal error is detected.
     * @returns A shared pointer to a new differentiated expression.
     */
    virtual std::shared_ptr<Expression> tangDifferentiate() const = 0;
    /**
     * @brief Recursively accumulate all adjoints for this expression.
     * @details To start the differentiation process use ExpAssign::startTangDifferenciate().
     * @throws EigenADException if a semantic or an internal error is detected.
     * @param tempAdj A temporary adjoint.
     * @param allAdjoints A vector to store all accumulated adjoints.
     */
    virtual void adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const = 0; //accumulates all adjoints
    /**
     * @brief Create a deep copy of this expression.
     * @returns A shared pointer to the copy of the expression.
     */
    virtual std::shared_ptr<Expression> deepCopy() const = 0;
    /**
     * @brief Recursively check the semantics of this expression with respect to matrices and scalars.
     * @details Set resultType_ and throws EigenADException if necessary.
     * @throws EigenADException if a semantic error is detected.
     * @returns The result type of this expression.
     */
    virtual ExpressionResultType checkSemantics() = 0;
    /**
     * @brief Recursively set all the DimNodes of all matrices in this expression based on the operators used.
     * @throws EigenADException if an internal error is detected.
     * @param graph The DimensionGraph used for inferring dimensions.
     */
    virtual void setDimensionNodes(DimGraph& graph) = 0;
    /**
     * @brief Generate the matrix values for this expression.
     * @details Because of missing common subexpression elimination the cache is used to assign the same values to all ExpMatrix objects, that represent the same matrix.
     * @throws EigenADException if an internal error is detected.
     * @param matrixValuesCache The cache used to store the matrix values.
     */
    virtual void generateMatrixValues(MatrixCache& matrixValuesCache) = 0;
    /**
     * @brief Calculate the value of this expression.
     * @throws EigenADException if a semantic or an internal error is detected.
     * @param matrixValuesCache The cache that contains the matrix values.
     * @returns A variant that represents the result of the calculation.
     */
    virtual std::variant<ScalarValueType, MatrixType> calculate() const = 0;
    /**
     * @brief Get the resultType_ of this expression.
     * @returns The result type of this expression.
     */
    virtual ExpressionResultType getExpressionResultType() const;
    /**
     * @brief Set the resultType_ of this expression.
     * @param retType The result type to be set.
     */
    virtual void setReturnType(ExpressionResultType retType);
    /**
     * @brief Set and increment the ID of this expression.
     * @details Each Expression object has a unique id_.
     */
    virtual void setAndIncrementID();
    /**
     * @brief Get the id_ of this expression.
     * @returns The id_ of this expression.
     */
    virtual int getID() const;
    /**
     * @brief Get the hash ID of this expression.
     * @details Each Expression object has a hash unique id_.  The character ":" should not be allowed in the input!.
     * @returns The hash ID of this expression.
     */
    virtual std::string getHashID() const;
    /**
     * @brief Get valid combinations of ExpressionResultTypes.
     * @details Each Expression object requires a unique combination of ExpressionResultTypes.
     * @returns The vector of a vector of valid ExpressionResultTypes.
     */
    virtual std::vector<std::vector<ExpressionResultType>> getRequiredTypes() const = 0;
    /**
     * @brief Get the name of this expression.
     * @details Each Expression has a unique name.
     * @returns The name of this expression.
     */
    virtual ExpressionType getExpressionType() const = 0;
    /**
     * @brief Create a row DimNode for this expression.
     * @details The resulting DimNode is based upon the hash id of this expression. If this function runs on a ExpMatrix object, a DimNodeMatrix is returned, which also contains the final output to check which DimNodeMatrix objects represent the same.
     * @returns A shared pointer to the created row DimNode.
     */
    virtual std::shared_ptr<DimNode> createRowDimNode() const;
    /**
     * @brief Create a column DimNode for this expression.
     * @details The resulting DimNode is based upon the hash id of this expression. If this function runs on a ExpMatrix object, a DimNodeMatrix is returned, which also contains the final output to check which DimNodeMatrix objects represent the same.
     * @returns A shared pointer to the created column DimNode.
     */
    virtual std::shared_ptr<DimNode> createColDimNode() const;
    /**
     * @brief Set the order of derivative for this expression.
     * @param ord The order of derivative to be set.
     */
    static void setDerivativeOrder(int ord);
    /**
    * @brief Get the order of derivative for this expression.
    * @return The order of derivative.
    */
    static int getDerivativeOrder();
    /**
     * @brief Set the optimized output flag.
     * @param value The value of the flag.
     */
    static void setOptimizeOutput(bool value);

    /**
     * @brief Get the optimized output flag.
     * @return The optimized output flag.
     */
    static bool getOptimizeOutput();
    /**
     * @brief Recursively pertubates all matrix values.
     * @details This function is used to calculate FDA.
     * @param amount The amount by which a matrix is pertubated;
     * @param matrixValuesCache The cache where all matrix values are stored is used to find the counterpart to the matrix.
     */
    virtual void perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) = 0;
    /**
     * @brief Recursively extracts all subexpressions into the given parameter.
     * @details Currently only extracts Cholesky Factorization and only supports application, directly into the input equation.
     * @param subexpressions The vector that after completion holds all subexpressions in the form of new assignments.
     */
    virtual void devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache) = 0;

    /**
     * @brief Recursively walks the expression tree and builds a new tree based on the original that has an ExpLowUpp object where an ExpCholesk object was.
     * @return The new modified tree.
     */
    virtual std::shared_ptr<Expression> transformExpCholeskToExpLowUpp() const = 0;
protected:
    /**
     * @brief The iterator ID for Expression objects.
     */
    static int iteratID;
    
    /**
     * @brief The flag to optimize the output.
     * @details Currently only llt() operations get a substitution string.
     */
    static bool optimizeOutput;

    /**
    * @brief The order of derivative for this expression.
    */
    static int derivativeOrder;
    /**
     * @brief The unique ID of this expression.
     */
    int id_ = -1;
    /**
     * @brief The result type of this expression.
     */
    ExpressionResultType resultType_ = ExpressionResultType::NotDetermined;
};
