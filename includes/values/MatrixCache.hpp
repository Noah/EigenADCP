#pragma once
#include <unordered_map>
#include <memory>
#include <string>
#include "EigenNS.hpp"
#include "expression/DerivativeType.hpp"

class ExpMatrix;

/**
 * @struct MatrixDescription
 * @brief Describes a matrix.
 * @details Contains various relevant attributes describing a matrix.
 */
struct MatrixDescription {
	std::string matrixNameAndSuffix;
	DerivativeType derType;
	int iteration;
	MatrixType matrixValue;
	std::string rowName;
	std::string colName;
};

/**
 * @class MatrixCache
 * @brief Stores and manages matrix descriptions and their corresponding values.
 * @details Provides methods for adding, accessing, and validating all matrices in the orignal expression and its derivatives.
 */
class MatrixCache {
public:


	/**
	 * @brief Default constructor.
	 */
	MatrixCache() = default;
	/**
	 * @brief Adds a new input entry one of the input matrix caches depending on the currentTraversal flag.
	 * @details Only called when traversing the tree and running into an ExpMatrix object.
	 * @param matrixFinalOut The final output of the matrix.
	 * @param name Name of the matrix.
	 * @param it Iteration number of the matrix.
	 * @param type Derivative type of the matrix.
	 * @param values Values of the matrix.
	 * @param rowDimName Output name of the row dimension.
	 * @param colDimName Output name of the column dimension.
	 */
	void addInputEntry(const std::string& matrixFinalOut, const std::string& name, const std::string& suffix, int it,
		DerivativeType type, const MatrixType& values, const std::string& rowDimName, const std::string& colDimName);
	/**
	 * @brief Adds a new input entry one of the ouput matrix caches depending on the currentTraversal flag.
	 * @param matrix The shared pointer to the output matrix.
	 */
	void addOutputEntry(const std::shared_ptr<ExpMatrix>& matrix);
	/**
	 * @brief Check if the cache contains a key. 
	 * @param key The key to check that is the final output of a matrix.
	 * @return True if the key is present, false otherwise.
	 */
	bool containsKey(const std::string& key) const;
	/**
	 * @brief Get the values of a matrix from the cache based on a key.
	 * @param key The key of the matrix that is the final output of a matrix.
	 * @return The matrix values.
	 */
	MatrixType getValuesOf(const std::string& key) const;
	/**
	 * @brief Check if the cache contains a key.
	 * @param key The key to check that is the final output of a subexpression of a ExpCholesk object.
	 * @return True if the key is present, false otherwise.
	 */
	bool containsLLTKey(const std::string& key) const;
	/**
	 * @brief Get the values of a cholesky factorization from the cache based on a key.
	 * @param key The key of the LLT object that is the final output of a subexpression of a ExpCholesk object.
	 * @return The cholesky factorization value including the matrix it is based on.
	 */
	std::pair<MatrixType, Eigen::LLT<MatrixType>> getLLTValuesOf(const std::string& key) const;
	/**
	 * @brief Get the name substitution of an LLT key.
	 * @param key The key of the LLT object that is the final output of a subexpression of a ExpCholesk object.
	 * @return The counter of the substitution entry.
	 */
	size_t getLLTNameSubstCounterOf(const std::string& key) const;
	/**
	 * @brief Add an LLT entry to the cache.
	 * @param key The key of the LLT object that is the final output of a subexpression of a ExpCholesk object.
	 * @param leftSub The name substitution of the LLT subtree.
	 * @param llt The LLT value.
	 * @return The counter of the substitution entry.
	 */
	size_t addLLTEntry(const std::string& key, MatrixType lltBaseValues, Eigen::LLT<MatrixType> llt);

	/**
	 * @brief Validates the MatrixCache by comparing the calculated values.
	 *
	 * This function validates the MatrixCache by comparing the calculated values between tangent and adjoint matrices.
	 * It checks if the inner products of corresponding tangent and adjoint matrices match.
	 * If the calculated values deviate within the specified EPSILON threshold, the validation passes.
	 *
	 * @return True if the MatrixCache is valid, false otherwise.
	 */
	void validateDuality() const;

	/**
	 * @brief Generates the matrix declarations for the MatrixCache.
	 *
	 * This function generates the matrix declarations based on the entries in the MatrixCache.
	 * It iterates over the input and output caches and constructs the matrix declarations with their corresponding dimensions.
	 * The generated matrix declarations are returned as a string.
	 *
	 * @return A string containing the generated matrix declarations.
	 */
	std::string getMatrixDeclarations() const;

	/**
	 * @brief Get the string representation of the cache.
	 * @return The string representation.
	 */
	std::string toString() const;

	/**
	 * @brief Finds the counter part matrix to the given matrix.
	 * @returns The values of the found counter part matrix.
	 */
	MatrixType findCounterPartValueTo(const std::string& name, const std::string& suffix, int it) const;
private:
	//<left_->toString(), <nameSubstitution_, llt>>
	/**
	 * @brief Cache for storing Eigen::LLT objects with their corresponding counter and their left subexpression as a key.
	 */
	std::unordered_map<std::string, std::pair<size_t, std::pair<MatrixType, Eigen::LLT<MatrixType>>>> lltCache_;

	/**
	 * @brief Cache for storing input matrix descriptions of the primal expression paired with a key that is the final string representation of a matrix in the output.
	 */
	std::unordered_map<std::string, MatrixDescription> primalInputCache_;
	/**
	 * @brief Cache for storing output matrix descriptions of the primal expression paired with a key that is the final string representation of a matrix in the output.
	 */
	std::unordered_map<std::string, MatrixDescription> primalOutputCache_;
	/**
	 * @brief Cache for storing input matrix descriptions of all adjoint derivatives paired with a key that is the final string representation of a matrix in the output.
	 */
	std::unordered_map<std::string, MatrixDescription> adjointInputCache_;
	/**
	 * @brief Cache for storing output matrix descriptions of all adjoint derivatives paired with a key that is the final string representation of a matrix in the output.
	 */
	std::unordered_map<std::string, MatrixDescription> adjointOutputCache_;
	/**
	 * @brief Cache for storing input matrix descriptions of the tangent derivative paired with a key that is the final string representation of a matrix in the output.
	 */
	std::unordered_map<std::string, MatrixDescription> tangentInputCache_;
	/**
	 * @brief Cache for storing output matrix descriptions of the tangent derivative paired with a key that is the final string representation of a matrix in the output.
	 */
	std::unordered_map<std::string, MatrixDescription> tangentOutputCache_;

};

