#pragma once
#include <exception>
#include <string>
#include <vector>
#include "expression/ExpressionResultType.hpp"
#include "expression/FDAType.hpp"
#include "EigenNS.hpp"

/**
 * @class EigenADException
 * @brief Custom base exception class for EigenAD.
 *
 * @details This class inherits from the standard exception class. It is intended to act as a base class for
 * more specific EigenAD exception classes.
 */
class EigenADException : public std::exception {
public:
    EigenADException() = default;

    virtual ~EigenADException() = default;

    /**
     * @brief Returns a character string describing the exception.
     *
     * This function overrides the what() function from the base class std::exception.
     *
     * @return String describing the exception.
     */
    virtual const char* what() const noexcept override {
        return detailedOutMessage_.c_str();
    }

    /**
     * @brief Returns a user-friendly exception name.
     *
     * This is a pure virtual function that must be overridden in any non-abstract class that
     * directly or indirectly inherits from this class.
     *
     * @return String with a user-friendly exception name.
     */
    virtual std::string getDisplayExceptionName() const = 0;
    /**
     * @brief Returns a user-friendly exception error message.
     *
     * This is a pure virtual function that must be overridden in any non-abstract class that
     * directly or indirectly inherits from this class.
     *
     * @return String with a user-friendly exception error message.
     */
    virtual std::string getDisplayExceptionErrorMessage() const = 0;

protected:
    /**
     * @brief The error message to be displayed when the exception is thrown.
     * @details This string is constructed from the other member variables to provide a detailed explanation of the exception.
     */
    std::string detailedOutMessage_;
};

/**
 * @class CholeskyFactorizationZeroInput
 * @brief Custom exception class for when a subexpression in a cholesky factorization results in a zero matrix.
 * @details Thrown when there is a semantic error in the input expression, but not until calculation.
 */
class CholeskyFactorizationZeroInput : public EigenADException {
public:
    /**
     * @brief Constructor.
     * @param expName Name of the expression where the exception occurred.
     * @param expOutput The expression represented as a string where the exception occurred.
     * @param addMsg Additional message for the exception.
     */
    CholeskyFactorizationZeroInput(ExpressionType expName, const std::string& expOutput, const std::string& addMsg) {
        expType_ = expName;
        expOutput_ = expOutput;
        addMsg_ = addMsg;
        detailedOutMessage_ = "CholeskyFactorizationZeroInput in: " + addMsg_ + "\nThe left subexpression in the following Cholesky Factorization yields a zero matrix:\n" +
            expOutput_ + "\nIt should result in a symmetric positive definite matrix.";
    }

    std::string getDisplayExceptionErrorMessage() const override {
        std::string out = "The left subexpression in the following Cholesky Factorization yields a zero matrix : \n" +
            expOutput_ + "\nIt should result in a symmetric positive definite matrix.";

        return out;
    }

    std::string getDisplayExceptionName() const override {
        return "Zero Input in Cholesky Factorization";
    }

private:
    /**
     * @brief Name of the expression where the exception occurred.
     */
    ExpressionType expType_;
    /**
     * @brief The expression represented as a string where the exception occurred.
     */
    std::string expOutput_;
    /**
     * @brief Additional message for the exception.
     * @details This string can be used to provide more context or details about the exception.
     */
    std::string addMsg_;
};

/**
 * @class InvalidMatrixExpressionException
 * @brief Custom exception class for invalid matrix expressions.
 * @details Thrown when there is a semantic error in the input expression.
 */
class InvalidMatrixExpressionException : public EigenADException {
public:

    /**
     * @brief Constructor.
     * @param expName Name of the expression where the exception occurred.
     * @param existingTypes Vector of existing types in the expression.
     * @param requiredTypes Vector of vectors representing required types in the expression.
     * @param expOutput The expression represented as a string where the exception occurred.
     * @param addMsg Additional message for the exception.
     */
    InvalidMatrixExpressionException(ExpressionType expName, const std::vector<ExpressionResultType>& existingTypes,
        const std::vector<std::vector<ExpressionResultType>> requiredTypes, const std::string& expOutput, const std::string& addMsg) {
        expType_ = expName;
        existingTypes_ = existingTypes;
        requiredTypes_ = requiredTypes;
        expOutput_ = expOutput;
        addMsg_ = addMsg;

        detailedOutMessage_ = "InvalidMatrixExpressionException in: " + addMsg_ + "\n";
        detailedOutMessage_= ExpressionTypeToString(expType_) + " has the following subexpression result types: ";
        for (ExpressionResultType type : existingTypes_) {
            detailedOutMessage_= detailedOutMessage_+ ExpressionResultTypeToString(type) + " ";
        }
        detailedOutMessage_= detailedOutMessage_+ "\n" + "and the following are required: ";
        for (const auto& vec : requiredTypes_) {
            for (ExpressionResultType type : vec) {
                detailedOutMessage_= detailedOutMessage_+ ExpressionResultTypeToString(type) + " ";
            }
            detailedOutMessage_= detailedOutMessage_+ " ";
        }
        detailedOutMessage_= detailedOutMessage_+ "\n" + "in " + expOutput_;

    }

    std::string getDisplayExceptionErrorMessage() const override {
        std::string out = "The highest " + ExpressionTypeToString(expType_) + " in " + expOutput_ + " has the following result type(s) for its subexpression(s):\n";
        for (ExpressionResultType type : existingTypes_) {
            out += ExpressionResultTypeToString(type) + " ";
        }
        out += "\nBut the following are/is required:\n";
        for (const auto& vec : requiredTypes_) {
            for (ExpressionResultType type : vec) {
                out += ExpressionResultTypeToString(type) + " ";
            }
            out += "  ";
        }

        return out;
    }

    std::string getDisplayExceptionName() const override {
        return "Invalid Matrix Expression";
    }

private:
    /**
     * @brief Name of the expression where the exception occurred.
     */
    ExpressionType expType_;
    /**
     * @brief Vector of existing types in the expression.
     * @details Each element represents the type of a subexpression in the input expression.
     */
    std::vector<ExpressionResultType> existingTypes_;
    /**
     * @brief Vector of vectors representing required types in the expression.
     * @details Each sub-vector represents a set of types that are required for a subexpression in the input expression.
     */
    std::vector<std::vector<ExpressionResultType>> requiredTypes_;
    /**
     * @brief The expression represented as a string where the exception occurred.
     */
    std::string expOutput_;
    /**
     * @brief Additional message for the exception.
     * @details This string can be used to provide more context or details about the exception.
     */
    std::string addMsg_;
};


/**
 * @class InvalidResultTypeException
 * @brief Custom exception class for invalid return values.
 * @details Thrown when there is an internal error with result types.
 */
class InvalidResultTypeException : public EigenADException {
public:
    /**
     * @brief Constructor.
     * @param expName Name of the expression where the exception occurred.
     * @param existingTypes Vector of existing types in the expression.
     * @param requiredTypes Vector of vectors representing required types in the expression.
     * @param expOutput The expression represented as a string where the exception occurred.
     * @param addMsg Additional message for the exception.
     */
    InvalidResultTypeException(ExpressionType expName, const std::vector<ExpressionResultType>& existingTypes,
        const std::vector<std::vector<ExpressionResultType>> requiredTypes, const std::string& expOutput, const std::string& addMsg) {
        expType_ = expName;
        existingTypes_ = existingTypes;
        requiredTypes_ = requiredTypes;
        expOutput_ = expOutput;
        addMsg_ = addMsg;
        detailedOutMessage_ = "InvalidResultTypeException in: " + addMsg_ + "\n";
        detailedOutMessage_ = ExpressionTypeToString(expType_) + " has the following subexpression result types: ";
        for (ExpressionResultType type : existingTypes_) {
            detailedOutMessage_ = detailedOutMessage_ + ExpressionResultTypeToString(type) + " ";
        }
        detailedOutMessage_ = detailedOutMessage_ + "\n" + "and the following are required: ";
        for (const auto& vec : requiredTypes_) {
            for (ExpressionResultType type : vec) {
                detailedOutMessage_ = detailedOutMessage_ + ExpressionResultTypeToString(type) + " ";
            }
            detailedOutMessage_ = detailedOutMessage_ + " ";
        }
        detailedOutMessage_ = detailedOutMessage_ + "\n" + "in " + expOutput_;
        

    }

    std::string getDisplayExceptionErrorMessage() const override {
        std::string out = "The highest " + ExpressionTypeToString(expType_) + " in " + expOutput_ + " has the following result type(s) for its subexpression(s):\n";
        for (ExpressionResultType type : existingTypes_) {
            out += ExpressionResultTypeToString(type) + " ";
        }
        out += "\nBut the following are required:";
        for (const auto& vec : requiredTypes_) {
            for (ExpressionResultType type : vec) {
                out += ExpressionResultTypeToString(type) + " ";
            }
            out += "  ";
        }

        return out;
    }

    std::string getDisplayExceptionName() const override {
        return "Internal Error: Invalid Result Type";
    }

private:
    /**
     * @brief Name of the expression where the exception occurred.
     */
    ExpressionType expType_;

    /**
     * @brief Vector of existing types in the expression.
     * @details Each element represents the type of a subexpression in the input expression.
     */
    std::vector<ExpressionResultType> existingTypes_;

    /**
     * @brief Vector of vectors representing required types in the expression.
     * @details Each sub-vector represents a set of types that are required for a subexpression in the input expression.
     */
    std::vector<std::vector<ExpressionResultType>> requiredTypes_;

    /**
     * @brief The expression represented as a string where the exception occurred.
     */
    std::string expOutput_;

    /**
     * @brief Additional message for the exception.
     * @details This string can be used to provide more context or details about the exception.
     */
    std::string addMsg_;
};

/**
 * @class UninitializedValuesException
 * @brief Custom exception class for uninitialized values.
 * @details Thrown when an expression contains uninitialized values.
 */
class UninitializedValuesException : public EigenADException {
public:
    /**
     * @brief Constructor.
     * @param expName Name of the expression where the exception occurred.
     * @param expOutput The expression represented as a string where the exception occurred.
     * @param addMsg Additional message for the exception.
     */
    UninitializedValuesException(ExpressionType expName, const std::string& expOutput, const std::string& addMsg){
        expType_ = expName;
        expOutput_ = expOutput;
        addMsg_ = addMsg;
        detailedOutMessage_ = "UninitializedValuesException in: " + addMsg_ + "\n";
        detailedOutMessage_ = ExpressionTypeToString(expType_) + " has uninitialized values in " + expOutput_;
    }

    std::string getDisplayExceptionErrorMessage() const override {
        return detailedOutMessage_; //internal error: no user friendly message needed
    }

    std::string getDisplayExceptionName() const override {
        return "Internal Error: Uninitialized Values";
    }

private:
    /**
     * @brief Name of the expression where the exception occurred.
     */
    ExpressionType expType_;

    /**
     * @brief The expression represented as a string where the exception occurred.
     */
    std::string expOutput_;

    /**
     * @brief Additional message for the exception.
     * @details This string can be used to provide more context or details about the exception.
     */
    std::string addMsg_;
};

/**
 * @class InvalidDimensionsException
 * @brief Custom exception class for an internal error when assigning matrix values.
 * @details Thrown when an existing matrix receives matrix values whose dimensions are incompatible.
 */
class InvalidDimensionsException : public EigenADException {
public:
    /**
     * @brief Constructor.
     * @param matrixToString String representation of the matrix where the exception occurred.
     * @param assignedRows Number of rows tried to assign to the matrix.
     * @param assignedCols Number of columns tried to assign to the matrix.
     * @param matrixRows Number calculated of rows of the matrix.
     * @param matrixCols Number of calculated columns of the matrix.
     */
    InvalidDimensionsException(const std::string& matrixToString, Eigen::Index assignedRows, Eigen::Index assignedCols, int matrixRows, int matrixCols) {
        matrixmatrixName_ = matrixToString;
        assignedRows_ = assignedRows;
        assignedCols_ = assignedCols;
        matrixRows_ = matrixRows;
        matrixCols_ = matrixCols;
        detailedOutMessage_ = "InvalidDimensionsException: Tried to assign matrix values of size (" + std::to_string(assignedRows_) + "," + std::to_string(assignedCols_) + ") to " +
            matrixmatrixName_ + ", when Dimension objects command matrix values of size (" + std::to_string(matrixRows_) + "," + std::to_string(matrixCols_) + ").";
    }

    std::string getDisplayExceptionErrorMessage() const override {
        return detailedOutMessage_; //internal error: no user friendly message needed
    }

    std::string getDisplayExceptionName() const override {
        return "Internal Error: Invalid Dimensions";
    }

private:
    /**
     * @brief String representation of the matrix where the exception occurred.
     */
    std::string matrixmatrixName_;

    /**
     * @brief Number of rows tried to assign to the matrix.
     */
    Eigen::Index assignedRows_;

    /**
     * @brief Number of columns tried to assign to the matrix.
     */
    Eigen::Index assignedCols_;

    /**
     * @brief Number calculated of rows of the matrix.
     */
    int matrixRows_;

    /**
     * @brief Number of calculated columns of the matrix.
     */
    int matrixCols_;
};

/**
 * @class InvalidDerivativeOrderException
 * @brief Custom exception class for an invalid user specified derivative order.
 */
class InvalidDerivativeOrderException : public EigenADException {
public:
    /**
     * @brief Constructor.
     * @param highestOrderFound Pair containing the name of the matrix and the highest derivative order found in the matrix.
     * @param userOrder The user-specified derivative order.
     */
    InvalidDerivativeOrderException(std::pair<std::string, int> highestOrderFound, int userOrder) {
        matrixName_ = highestOrderFound.first;
        highestOrderFound_ = highestOrderFound.second;
        userOrder_ = userOrder;
        
        detailedOutMessage_ = "InvalidDerivativeOrderException: User defined derivative order was " + std::to_string(userOrder_) + ", " +
            "which is smaller or equal to the highest order " + std::to_string(highestOrderFound_) + " found in " + matrixName_ + ".";
    }

    std::string getDisplayExceptionErrorMessage() const override {
        std::string out = "The highest order " + std::to_string(highestOrderFound_) + " found in " + matrixName_ + " is bigger or equal than the user defined derivative order of " + std::to_string(userOrder_) + ".";
        return out;
    }

    std::string getDisplayExceptionName() const override {
        return "Invalid Derivative Order";
    }

private:
    /**
     * @brief The name of the matrix where the highest order was found.
     */
    std::string matrixName_;

    /**
     * @brief The user-specified derivative order.
     */
    int userOrder_;

    /**
     * @brief The highest derivative order found in the matrix.
     */
    int highestOrderFound_;
};

/**
 * @class NoAssignmentRecognizedException.
 * @brief Custom exception class for an unrecognized / empty input assignment.
 */
class NoAssignmentRecognizedException : public EigenADException {
public:
    /**
     * @brief Constructor.
     */
    NoAssignmentRecognizedException() {
        detailedOutMessage_ = "NoAssignmentRecognizedException: No input assignment was given or it was not recognized.";
    }

    std::string getDisplayExceptionErrorMessage() const override {
        return "No input assignment was given or it was not recognized.";
    }

    std::string getDisplayExceptionName() const override {
        return "No Input Detected";
    }

private:
};

/**
 * @class ListenerHasErrorException
 * @brief Custom exception class for errors happening during lexing or parsing the input expression.
 */
class ListenerHasErrorException : public EigenADException {
public:
    /**
     * @brief Constructor.
     * @param errorMsgs The list of error messages encountered during lexing or parsing.
     */
    ListenerHasErrorException(const std::vector<std::string>& errorMsgs){
        errorMsgs_ = errorMsgs;
        std::string errorMessages_;
        for (std::string& msg : errorMsgs_) {
            errorMessages_ += msg + "\n";
        }
        detailedOutMessage_ = "ListenerHasErrorException: The listener encountered the following errors: " + errorMessages_;
    }

    std::string getDisplayExceptionErrorMessage() const override {
        std::string out = "The following errors occurred during parsing:\n";
        int i = 0;
        for (std::string msg : errorMsgs_) {
            out += msg + "\n";
            i++;
            if (i > 20) {
                out += "and " + std::to_string(errorMsgs_.size() - i) + " more.";
                break; //only output the first 20 errors
            }  
        }
        return out;
    }

    std::string getDisplayExceptionName() const override {
        return "Parsing Error";
    }
private:

    /**
     * @brief All error messages encountered during lexing or parsing.
     */
    std::vector<std::string> errorMsgs_;;
};


/**
 * @class DualityValidationFailedException
 * @brief Custom exception class for validation failures.
 * @details Thrown when a validation error occurs, such as when the left side of the validation does not equal the right side within a certain tolerance.
 */
class DualityValidationFailedException : public EigenADException {
public:
    /**
     * @brief Constructor.
     * @param left The value of the left side of the validation equation.
     * @param right The value of the right side of the validation equation.
     * @param absDiff The absolute difference between the left and right sides of the validation equation.
     * @param percDiff The percentage difference between the left and right sides of the validation equation.
     */
    DualityValidationFailedException(double left, double right, double absDiff, double percDiff) {
        left_ = left;
        right_ = right;
        absDiff_ = absDiff;
        percDiff_ = percDiff;
        detailedOutMessage_ = "DualityValidationFailedException: The Validation failed at " + std::to_string(left_) + "=?=" + std::to_string(right_) + ",\n" +
            "with an absolute difference of " + std::to_string(absDiff_) + " and a percentage difference of " + std::to_string(percDiff_) + " > EPSILON.";
    }

    std::string getDisplayExceptionErrorMessage() const override {
        return detailedOutMessage_;
    }

    std::string getDisplayExceptionName() const override {
        return "Internal Error: Validation Failed";
    }

private:
    /**
     * @brief The value of the left side of the validation equation.
     */
    double left_;

    /**
     * @brief The value of the right side of the validation equation.
     */
    double right_;

    /**
     * @brief The absolute difference between the left and right sides of the validation equation.
     */
    double absDiff_;

    /**
     * @brief The percentage difference between the left and right sides of the validation equation.
     */
    double percDiff_;
};

/**
 * @class FDAValidationFailedException
 * @brief Custom exception class for validation failures with finite differences.
 * @details Thrown when a validation error occurs in the context of finite differences, e.g., when the average absolute or percentage difference is too high.
 */
class FDAValidationFailedException : public EigenADException {
public:
    /**
     * @brief Constructor.
     * @param fdaType forward, backward or central FDA.
     * @param avgAbsDiff The average absolute difference.
     * @param avgPercDiff The average percentage difference.
     */
    FDAValidationFailedException(FDAType fdaType, double avgAbsDiff, double avgPercDiff) {
        avgAbsDiff_ = avgAbsDiff;
        avgPercDiff_ = avgPercDiff;
        fdaType_ = fdaType;
        detailedOutMessage_ = "FDAValidationFailedException: The " + FDATypeToString(fdaType_) +" Validation of a (nested) (sub)expression failed with an average absolute difference of " +
            std::to_string(avgAbsDiff_) + " and an average percentage difference of " + std::to_string(avgPercDiff_) + " > FDA_EPSILON.";
    }

    std::string getDisplayExceptionErrorMessage() const override {
        return "The " + FDATypeToString(fdaType_) + " FDA Validation of a (nested) (sub)expression failed with an average absolute difference of " +
            std::to_string(avgAbsDiff_) + " and an average percentage difference of " + std::to_string(avgPercDiff_) + " > FDA_EPSILON.";
    }

    std::string getDisplayExceptionName() const override {
        return "FDA Validation Failed";
    }

private:
    /**
     * @brief The type of the FDA.
     */
    FDAType fdaType_;
    /**
     * @brief The average absolute difference.
     */
    double avgAbsDiff_;

    /**
     * @brief The average percentage difference.
     */
    double avgPercDiff_;
};

/**
 * @class CallOfUndefinedFunctionException
 * @brief Custom exception class for a call to an undefined or unimplemented function.
 * @details Thrown when a call to an unimplemented or not functional method occurs.
 */
class CallOfUndefinedFunctionException : public EigenADException {
public:
    /**
     * @brief Constructor.
     * @param expName Name of the expression where the exception occurred.
     * @param functionName The name of the function that was called.
     * @param addMsg Additional message for the exception.
     */
    CallOfUndefinedFunctionException(ExpressionType expName, const std::string& functionName, const std::string& addMsg) {
        expName_ = expName;
        functionName_ = functionName;
        addMsg_ = addMsg;
        detailedOutMessage_ = "CallOfUndefinedFunctionException: The function " + functionName_ + " in " + ExpressionTypeToString(expName) + " is not fully implemented or functional.\n" +
            "Reason(s): " + addMsg_;
    }

    std::string getDisplayExceptionErrorMessage() const {
        return detailedOutMessage_;
    }

    std::string getDisplayExceptionName() const {
        return "Internal Error: CallOfUndefinedFunctionException";
    }
private:
    /**
     * @brief Name of the expression where the exception occurred.
     */
    ExpressionType expName_;
    /**
     * @brief The name of the function where the exception occured..
     */
    std::string functionName_;
    /**
     * @brief Additional message for the exception that provides the reason why said function is not defined.
     * @details This string can be used to provide more context or details about the exception.
     */
    std::string addMsg_;
};
