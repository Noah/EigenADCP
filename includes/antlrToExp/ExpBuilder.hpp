#pragma once
#include "EigenExpBaseVisitor.h"

/**
 * @class ExpBuilder
 * @brief Visitor for an antlr parse tree that builds expression objects.
 * @details Inherits from the EigenExpBaseVisitor. Each visit method returns a respective Expression object.
 */
class ExpBuilder : public EigenExpBaseVisitor {
public:
	/**
	 * @brief Visit method for scalar value identifiers.
	 * @param ctx Context of the scalar value identifier.
	 * @return An ExpScalar object wrapped inside std::any.
	 */
	std::any visitScalarValueID(EigenExpParser::ScalarValueIDContext* ctx) override;
	/**
	 * @brief Visit method for matrix identifiers.
	 * @param ctx Context of the matrix identifier.
	 * @return An ExpMatrix object wrapped inside std::any.
	 */
	std::any visitMatrixID(EigenExpParser::MatrixIDContext* ctx) override;

	/**
	 * @brief Visit method for the parent nodes of a scalar value.
	 * @param ctx Context of the parent nodes of a scalar value.
	 * @return An Expression object wrapped inside std::any that is returned by another function call.
	 */
	std::any visitExpScalarValue(EigenExpParser::ExpScalarValueContext* ctx) override;

	/**
	 * @brief Visit method for assignments identifiers.
	 * @param ctx Context of the assignments identifier.
	 * @return A vector of Expressions wrapped inside std::any.
	 */
	std::any visitAssignments(EigenExpParser::AssignmentsContext* ctx) override;
	/**
	 * @brief Visit method for assignment identifiers.
	 * @param ctx Context of the assignment identifier.
	 * @return An ExpAssign object wrapped inside std::any.
	 */
	std::any visitExpAssign(EigenExpParser::ExpAssignContext* ctx) override;
	/**
	 * @brief Visit method for parenthesis identifiers.
	 * @param ctx Context of the parenthesis identifier.
	 * @return An ExpParenth object wrapped inside std::any.
	 */
	std::any visitExpParenth(EigenExpParser::ExpParenthContext* ctx) override;

	/**
	 * @brief Visit method for the parent nodes of a matrix identifier.
	 * @param ctx Context of the parent nodes of a matrix identifier.
	 * @return An Expression object wrapped inside std::any that is returned by another function call.
	 */
	std::any visitExpMatrix(EigenExpParser::ExpMatrixContext* ctx) override;
	/**
	 * @brief Visit method for addition identifiers.
	 * @param ctx Context of the addition identifier.
	 * @return An ExpAdd object wrapped inside std::any.
	 */
	std::any visitExpAdd(EigenExpParser::ExpAddContext* ctx) override;
	/**
	 * @brief Visit method for negation identifiers.
	 * @param ctx Context of the negation identifier.
	 * @return An ExpNeg object wrapped inside std::any.
	 */
	std::any visitExpNeg(EigenExpParser::ExpNegContext* ctx) override;

	/**
	 * @brief Visit method for affirmation identifiers.
	 * @param ctx Context of the affirmation identifier.
	 * @return An Expression object wrapped inside std::any that is returned by another function call.
	 */
	std::any visitExpPos(EigenExpParser::ExpPosContext* ctx) override;

	/**
	 * @brief Visit method for transposition identifiers.
	 * @param ctx Context of the transposition identifier.
	 * @return An ExpTrans object wrapped inside std::any.
	 */
	std::any visitExpTrans(EigenExpParser::ExpTransContext* ctx) override;
	/**
	 * @brief Visit method for subtraction identifiers.
	 * @param ctx Context of the subtraction identifier.
	 * @return An ExpSub object wrapped inside std::any.
	 */
	std::any visitExpSub(EigenExpParser::ExpSubContext* ctx) override;
	/**
	 * @brief Visit method for multiplication identifiers.
	 * @param ctx Context of the multiplication identifier.
	 * @return An ExpMult object wrapped inside std::any.
	 */
	std::any visitExpMult(EigenExpParser::ExpMultContext* ctx) override;
	/**
	 * @brief Visit method for cholesky factorization and linear solving identifiers.
	 * @param ctx Context of the cholesky factorization and linear solving identifier.
	 * @return An ExpCholesk object wrapped inside std::any.
	 */
	std::any visitExpCholesk(EigenExpParser::ExpCholeskContext* ctx) override;

	 /**
	  * @brief Returns the highest order of the expressions encountered.
	  * @return Highest order of the expressions.
	  */
	std::pair<std::string, int> getHighestOrder() const;

	/**
	 * @brief Returns the full text name of the aliased variable.
	 * @returns The full text name of the aliased variable.
	 */
	std::string getAliasedName() const;
private:
	/**
	 * @brief Highest order of the expressions encountered.
	 * @details Stores the matrix and the order as a pair.
	 */
	std::pair<std::string, int> highestOrder_;

	/**
	 * @brief The full text name of the variable that is aliased (on both the input and the output of the original expression).
	 */
	std::string aliasedFullText_ = "NONE";
};