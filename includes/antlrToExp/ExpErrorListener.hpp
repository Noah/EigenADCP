#pragma once
#include <antlr4-runtime.h>
#include <string>
#include <vector>

using namespace antlr4;

/**
 * @class ExpErrorListener
 * @brief Custom error listener for ANTLR4.
 * @details Inherits from the ANTLRErrorListener. Used to customize error handling during parsing and lexing.
 */
class ExpErrorListener : public antlr4::ANTLRErrorListener {
public:
	/**
	* @brief Handles syntax errors.
	* @param recognizer Pointer to the recognizer where the error occurred.
	* @param offendingSymbol The offending token.
	* @param line Line number where the error occurred.
	* @param charPositionInLine Position in the line where the error occurred.
	* @param msg Error message.
	* @param e Exception pointer associated with the error.
	*/
	void syntaxError(Recognizer* recognizer, Token* offendingSymbol, size_t line,
		size_t charPositionInLine, const std::string& msg, std::exception_ptr e) override;
	/**
	 * @brief Overridden method to report ambiguities.
	 * @details This method usally never gets called.
	 * @param recognizer Pointer to the parser recognizer.
	 * @param dfa Reference to the DFA.
	 * @param startIndex Start index where the ambiguity is detected.
	 * @param stopIndex Stop index where the ambiguity is detected.
	 * @param exact Boolean value indicating whether the ambiguity is exact.
	 * @param ambigAlts BitSet representing ambiguous alternatives.
	 * @param configs Pointer to ATN configuration set.
	 */
	void reportAmbiguity(Parser* recognizer, const dfa::DFA& dfa, size_t startIndex, size_t stopIndex, bool exact,
		const antlrcpp::BitSet& ambigAlts, atn::ATNConfigSet* configs) override;
	/**
	 * @brief Overridden method to report full context attempts.
	 * @details This method usally never gets called.
	 * @param recognizer Pointer to the parser recognizer.
	 * @param dfa Reference to the DFA.
	 * @param startIndex Start index where the context was attempted.
	 * @param stopIndex Stop index where the context was attempted.
	 * @param conflictingAlts BitSet representing conflicting alternatives.
	 * @param configs Pointer to ATN configuration set.
	 */
	void reportAttemptingFullContext(Parser* recognizer, const dfa::DFA& dfa, size_t startIndex, size_t stopIndex,
		const antlrcpp::BitSet& conflictingAlts, atn::ATNConfigSet* configs) override;
	/**
	 * @brief Overridden method to report context sensitivity.
	 * @details This method usally never gets called.
	 * @param recognizer Pointer to the parser recognizer.
	 * @param dfa Reference to the DFA.
	 * @param startIndex Start index where the context sensitivity was detected.
	 * @param stopIndex Stop index where the context sensitivity was detected.
	 * @param prediction The prediction made by the parser.
	 * @param configs Pointer to ATN configuration set.
	 */
	void reportContextSensitivity(Parser* recognizer, const dfa::DFA& dfa, size_t startIndex, size_t stopIndex,
		size_t prediction, atn::ATNConfigSet* configs) override;
	/**
	 * @brief Returns whether an error has occurred during parsing.
	 *
	 * @return True if an error has occurred, false otherwise.
	 */
	bool getHasError();
	/**
	 * @brief Returns all error messages that have been recorded.
	 *
	 * @return Vector of error messages.
	 */
	std::vector<std::string> getErrorMsgs();
private:
	/**
	 * @brief Indicates whether an error has occurred.
	 */
	bool hasError_ = false;
	/**
	 * @brief Stores all error messages.
	 */
	std::vector<std::string> errors_;
	/**
	 * @brief Helper function to add an error message to the list of errors.
	 * @param msg Error message to add.
	 */
	void addErrorMsg(const std::string& msg);
};
