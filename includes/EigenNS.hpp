#pragma once

#include <Eigen/Dense>
#include <random>

namespace Eigen {

    /**
     * @brief Alias for Eigen::Matrix that represents a vector.
     * @tparam T The type of the vector elements.
     * @tparam N The size of the vector, set to dynamic by default.
     */
    template<typename T, int N = Dynamic>
    using vector_t = Matrix<T, N, 1>;

    /**
     * @brief Alias for Eigen::Matrix that represents a matrix.
     * @tparam T The type of the matrix elements.
     * @tparam M The number of rows in the matrix, set to dynamic by default.
     * @tparam N The number of columns in the matrix, set to M by default.
     */
    template<typename T, int M = Dynamic, int N = M>
    using matrix_t = Matrix<T, M, N>;
}

/**
 * @brief Defines a type alias T for double, which may be used for scalar values.
 */
using ScalarValueType = double;

/**
 * @brief Defines a type alias for a dynamic Eigen matrix with ScalarValueType elements..
 */
using MatrixType = Eigen::matrix_t<ScalarValueType>;

/**
 * @brief Defines a small positive constant, EPSILON, as a tolerance for comparing.
 */
constexpr const double EPSILON = 1e-8;

/**
 * @brief The string is used as a prefix for identifiers related to LLT (Cholesky) factorization.
 */
const std::string LLT_SUBST = "LLT_";

/**
 * @brief Standard value of nameSubstitution_ for ExpCholesk objects.
 */
const std::string NO_CHOLESK_SUBST = "NO_SUB_NAME";

/**
 * @brief The string that is used as a prefix for identifiers related to the output of matrix declarations.
 */
const std::string EIGEN_MATRIX_NAMESPACE = "Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>";

/**
 * @brief The string that is used as a prefix for identifiers related to the output of dimensions.
 */
const std::string DIMENSION_PREFIX = "dim_";

/**
 * @brief The amount that is used to calculate finite differences.
 */
constexpr const double FDA_AMOUNT = 1e-8;

/**
 * @brief The amount that Finite Difference Approximations can differ.
 */
constexpr const double FDA_EPSILON = 1e-4;
