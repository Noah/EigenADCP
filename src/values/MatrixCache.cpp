#include "values/MatrixCache.hpp"
#include <unordered_map>
#include <memory>
#include <string>
#include "EigenNS.hpp"
#include "expression/DerivativeType.hpp"
#include "expression/terminal/ExpMatrix.hpp"
#include "exception/EigenADException.hpp"
#include <wx/msgdlg.h>

void MatrixCache::addInputEntry(const std::string& matrixFinalOut, const std::string& name, const std::string& suffixes, int it,
	DerivativeType type, const MatrixType& values, const std::string& rowDimName, const std::string& colDimName)
{
	MatrixDescription entry;
	entry.matrixNameAndSuffix = name + suffixes;
	entry.derType = type;
	entry.iteration = it;
	entry.matrixValue = values;
	entry.rowName = rowDimName;
	entry.colName = colDimName;


	switch (type) {
	case DerivativeType::Primal:
		primalInputCache_.emplace(matrixFinalOut, entry);
		break;
	case DerivativeType::Adjoint:
		adjointInputCache_.emplace(matrixFinalOut, entry);
		break;
	case DerivativeType::Tangent:
		tangentInputCache_.emplace(matrixFinalOut, entry);
		break;
	default:
		break;
	}

}

void MatrixCache::addOutputEntry(const std::shared_ptr<ExpMatrix>& matrix) {

	MatrixDescription entry;
	entry.matrixNameAndSuffix = matrix->getName() + matrix->getSuffixes();
	entry.derType = matrix->getMode();
	entry.iteration = matrix->getIteration();
	entry.matrixValue = matrix->getMatrixValues();
	entry.rowName = matrix->getRowDim()->toString();
	entry.colName = matrix->getColDim()->toString();

	switch (entry.derType) {
	case DerivativeType::Primal:
		primalOutputCache_.emplace(matrix->toString(), entry);
		break;
	case DerivativeType::Adjoint:
		adjointOutputCache_.emplace(matrix->toString(), entry);
		break;
	case DerivativeType::Tangent:
		tangentOutputCache_.emplace(matrix->toString(), entry);
		break;
	default:
		break;
	}
}

bool MatrixCache::containsKey(const std::string& key) const
{
	if (primalInputCache_.find(key) != primalInputCache_.end() ||
		adjointInputCache_.find(key) != adjointInputCache_.end() ||
		tangentInputCache_.find(key) != tangentInputCache_.end()) {
		return true;
	}
	return false;
}


MatrixType MatrixCache::getValuesOf(const std::string& key) const
{
	auto it = primalInputCache_.find(key);
	if (it != primalInputCache_.end()) {
		return it->second.matrixValue;
	}
	it = adjointInputCache_.find(key);
	if (it != adjointInputCache_.end()) {
		return it->second.matrixValue;
	}
	it = tangentInputCache_.find(key);
	if (it != tangentInputCache_.end()) {
		return it->second.matrixValue;
	}
	return MatrixType();
}


bool MatrixCache::containsLLTKey(const std::string& key) const
{
	return lltCache_.find(key) != lltCache_.end();
}

std::pair<MatrixType, Eigen::LLT<MatrixType>> MatrixCache::getLLTValuesOf(const std::string& key) const
{
	return lltCache_.find(key)->second.second;
}

size_t MatrixCache::getLLTNameSubstCounterOf(const std::string& key) const
{
	return lltCache_.find(key)->second.first;
}

size_t MatrixCache::addLLTEntry(const std::string& key, MatrixType lltBaseValues, Eigen::LLT<MatrixType> llt)
{
	size_t size = lltCache_.size();
	lltCache_.emplace(key, std::make_pair(size, std::make_pair(lltBaseValues, llt)));
	return size;
}

std::string MatrixCache::toString() const {
	std::string res = "";
	for (auto& outentry : primalOutputCache_) {
		res = res + "primalOutputCache: " + outentry.first + "\n";
	}
	for (auto& inentry : primalInputCache_) {
		res = res + "primalInputCache_: " + inentry.first + "\n";
	}
	for (auto& outentry : adjointOutputCache_) {
		res = res + "adjointOutputCache_: " + outentry.first + "\n";
	}
	for (auto& inentry : adjointInputCache_) {
		res = res + "adjointInputCache_: " + inentry.first + "\n";
	}
	for (auto& outentry : tangentOutputCache_) {
		res = res + "tangentOutputCache_: " + outentry.first + "\n";
	}
	for (auto& inentry : tangentInputCache_) {
		res = res + "tangentInputCache_: " + inentry.first + "\n";
	}
	for (auto& lltEntry : lltCache_) {
		res += "lltCache_: " + lltEntry.first + "\n";
	}
	return res;
}

MatrixType MatrixCache::findCounterPartValueTo(const std::string& name, const std::string& suffix, int it) const {
	//TODO

	auto values = tangentInputCache_.find(name + suffix + "_" + DerivTypeToString(DerivativeType::Tangent) + std::to_string(Expression::getDerivativeOrder()));
	if (values != tangentInputCache_.end()) {
		return values->second.matrixValue;
	}

	return MatrixType::Random(-1,-1);
}

void MatrixCache::validateDuality() const {
	double left = 0;
	double right = 0;

	//std::cout << toString() << std::endl;
	
	for (auto& adjOutEntry : adjointOutputCache_) {
		for (auto& tangInEntry : tangentInputCache_) {
			if (adjOutEntry.second.matrixNameAndSuffix == tangInEntry.second.matrixNameAndSuffix &&
				adjOutEntry.second.iteration == tangInEntry.second.iteration) {
				/*
				std::cout << "validation1: " << tangInEntry.second.matrixNameAndSuffix << "_" << DerivTypeToString(tangInEntry.second.derType) << tangInEntry.second.iteration << " with "
					<< adjOutEntry.second.matrixNameAndSuffix << "_" << DerivTypeToString(adjOutEntry.second.derType) << adjOutEntry.second.iteration << std::endl;
				*/
				right += tangInEntry.second.matrixValue.cwiseProduct(adjOutEntry.second.matrixValue).sum();
				
			}
		}
	}

	for (auto& tangOutEntry : tangentOutputCache_) {
		for (auto& adjInEntry : adjointInputCache_) {
			if (tangOutEntry.second.matrixNameAndSuffix == adjInEntry.second.matrixNameAndSuffix &&
				tangOutEntry.second.iteration == adjInEntry.second.iteration) {
				/*
				std::cout << "validation3: " << tangOutEntry.second.matrixNameAndSuffix << "_" << DerivTypeToString(tangOutEntry.second.derType) << tangOutEntry.second.iteration << " with "
					<< adjInEntry.second.matrixNameAndSuffix << "_" << DerivTypeToString(adjInEntry.second.derType) << adjInEntry.second.iteration << std::endl;
				*/
				left += tangOutEntry.second.matrixValue.cwiseProduct(adjInEntry.second.matrixValue).sum();
				
			}
		}
	}

	double absDiff = std::abs(left - right);
	double avgValue = (left + right) / 2.0;

	// Check for avgValue being 0 to avoid division by zero
	double percDiff = (avgValue != 0.0) ? (absDiff / avgValue) : 0.0;

	if (absDiff > EPSILON) {
		if (percDiff > EPSILON) {
			throw DualityValidationFailedException(left, right, absDiff, percDiff);
		}
	}
}

std::string MatrixCache::getMatrixDeclarations() const {
	std::string res = "//generated matrix declarations \n";
	res += EIGEN_MATRIX_NAMESPACE + " ";
	std::vector<std::string> alreadyOut;
	for (auto& primalInEntry : primalInputCache_) {
		res = res + primalInEntry.first + "(" + primalInEntry.second.rowName + ", " + primalInEntry.second.colName + "), ";
		alreadyOut.push_back(primalInEntry.first);
	}
	for (auto& adjointInEntry : adjointInputCache_) {
		auto it = std::find(alreadyOut.begin(), alreadyOut.end(), adjointInEntry.first);
		if (it == alreadyOut.end()) {
			res = res + adjointInEntry.first + "(" + adjointInEntry.second.rowName + ", " + adjointInEntry.second.colName + "), ";
			alreadyOut.push_back(adjointInEntry.first);
		}
	}
	for (auto& tangentInEntry : tangentInputCache_) {
		auto it = std::find(alreadyOut.begin(), alreadyOut.end(), tangentInEntry.first);
		if (it == alreadyOut.end()) {
			res = res + tangentInEntry.first + "(" + tangentInEntry.second.rowName + ", " + tangentInEntry.second.colName + "), ";
			alreadyOut.push_back(tangentInEntry.first);
		}
	}
	for (auto& primalOutEntry : primalOutputCache_) {
		auto it = std::find(alreadyOut.begin(), alreadyOut.end(), primalOutEntry.first);
		if (it == alreadyOut.end()) {
			res = res + primalOutEntry.first + "(" + primalOutEntry.second.rowName + ", " + primalOutEntry.second.colName + "), ";
			alreadyOut.push_back(primalOutEntry.first);
		}
	}
	for (auto& adjointOutEntry : adjointOutputCache_) {
		auto it = std::find(alreadyOut.begin(), alreadyOut.end(), adjointOutEntry.first);
		if (it == alreadyOut.end()) {
			res = res + adjointOutEntry.first + "(" + adjointOutEntry.second.rowName + ", " + adjointOutEntry.second.colName + "), ";
			alreadyOut.push_back(adjointOutEntry.first);
		}
	}
	for (auto& tangentOutEntry : tangentOutputCache_) {
		auto it = std::find(alreadyOut.begin(), alreadyOut.end(), tangentOutEntry.first);
		if (it == alreadyOut.end()) {
			res = res + tangentOutEntry.first + "(" + tangentOutEntry.second.rowName + ", " + tangentOutEntry.second.colName + "), ";
			alreadyOut.push_back(tangentOutEntry.first);
		}
	}
	res = res.substr(0, res.size() - 2);
	res += ";\n";
	if (!lltCache_.empty() && Expression::getOptimizeOutput()) {
		res += "//The following subexpressions should each yield an SPD matrix \n";
		for (size_t i = 0; i < lltCache_.size(); i++) {
			for (auto& lltEntry : lltCache_) {
				if (i == lltEntry.second.first) {
					res = res + "auto " + LLT_SUBST + std::to_string(lltEntry.second.first) + " = " + lltEntry.first + ".llt(); \n";
					break;
				}
			}
		}
	}
	return res;
}
