#include "EigenNS.hpp"
#include "expression/Expression.hpp"
#include "expression/terminal/ExpMatrix.hpp"
#include "exception/EigenADException.hpp"
#include "EigenExpLexer.h"
#include "EigenExpParser.h"
#include "antlrToExp/ExpBuilder.hpp"
#include "antlrToExp/ExpErrorListener.hpp"
#include "dimension/DimGraph.hpp"
#include "expression/ExpAssign.hpp"
#include "values/MatrixCache.hpp"
#include "expression/DerivativeType.hpp"

#include "GUI/MainFrame.hpp"

#include <wx/frame.h>
#include <string>
#include <wx/clipbrd.h>
#include <memory>

#include <wx/panel.h>
#include <wx/textctrl.h>
#include <wx/menu.h>
#include <wx/sizer.h>
#include <wx/msgdlg.h>
#include "GUI/innerPanels/DeclarationsPanel.hpp"
#include "GUI/innerPanels/AdjointsPanel.hpp"
#include "GUI/innerPanels/TangentPanel.hpp"

#include "expression/Expression.hpp"
#include <expression/binary/ExpAdd.hpp>
#include <expression/binary/ExpMult.hpp>
#include <expression/binary/ExpCholesk.hpp>
#include "expression/FDAType.hpp"

wxBEGIN_EVENT_TABLE(MainFrame, wxFrame)
    EVT_BUTTON(COPY_ADJOINTS_BUTTON, MainFrame::OnCopyAdjButtonClicked)
    EVT_BUTTON(COPY_TANGENT_BUTTON, MainFrame::OnCopyTangButtonClicked)
    EVT_BUTTON(START_BUTTON, MainFrame::OnStartButtonPressed)
    EVT_BUTTON(COPY_DECLARATIONS_BUTTON, MainFrame::OnCopyDeclButtonClicked)
    EVT_MENU(wxID_EXIT, MainFrame::OnQuit)
    EVT_MENU(wxID_HELP, MainFrame::OnDocumentation)
    EVT_MENU(wxID_ABOUT, MainFrame::OnAbout)
wxEND_EVENT_TABLE()

MainFrame::MainFrame(const wxString& title) : wxFrame(NULL, wxID_ANY, title) {

    SetBackgroundColour(wxColour(245, 245, 245));

    wxMenu* menuFile = new wxMenu;
    menuFile->Append(wxID_EXIT, "&Quit");

    wxMenu* menuHelp = new wxMenu;
    menuHelp->Append(wxID_HELP, "&GitLab");
    menuHelp->Append(wxID_ABOUT, "&About");

    wxMenuBar* menuBar = new wxMenuBar;
    menuBar->Append(menuFile, "File");
    menuBar->Append(menuHelp, "Help");

    SetMenuBar(menuBar);

    wxFlexGridSizer* fgs = new wxFlexGridSizer(6, 2, 9, 25);

    inputAssignDescr_ = new wxStaticText(this, wxID_ANY, "Input Assignment");
    inputOrderDescr_ = new wxStaticText(this, wxID_ANY, "Derivative Order");
    inputAssignmentBox_ = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(800, 60), wxTE_MULTILINE);
    condContrPan_ = new ConditionControlPanel(this);

    fgs->Add(inputAssignDescr_, 1, wxALIGN_CENTER | wxALL, 5);
    fgs->Add(inputAssignmentBox_, 1, wxEXPAND | wxALL, 5);
    fgs->Add(inputOrderDescr_, 1, wxALIGN_CENTER | wxALL, 5);
    fgs->Add(condContrPan_, 1, wxEXPAND | wxALL, 0);

    wxPanel* line = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(-1, 2));
    line->SetBackgroundColour(*wxBLACK);
    wxPanel* line2 = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(-1, 2));
    line2->SetBackgroundColour(*wxBLACK);
    fgs->Add(line, 1, wxEXPAND | wxALL, 5);
    fgs->Add(line2, 1, wxEXPAND | wxALL, 5);

    declPanel_ = new DeclarationsPanel(this);
    adjPanel_ = new AdjointsPanel(this);
    tangPanel_ = new TangentPanel(this);

    declarationsOutBox_ = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(800, 110), wxTE_MULTILINE | wxTE_READONLY);
    adjointsOutBox_ = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(800, 110), wxTE_MULTILINE | wxTE_READONLY);
    tangentOutBox_ = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(800, 110), wxTE_MULTILINE | wxTE_READONLY);

    fgs->Add(declPanel_, 1, wxEXPAND | wxALL, 5);
    fgs->Add(declarationsOutBox_, 1, wxEXPAND | wxALL, 5);

    fgs->Add(adjPanel_, 1, wxEXPAND | wxALL, 5);
    fgs->Add(adjointsOutBox_, 1, wxEXPAND | wxALL, 5);

    fgs->Add(tangPanel_, 1, wxEXPAND | wxALL, 5);
    fgs->Add(tangentOutBox_, 1, wxEXPAND | wxALL, 5);

    fgs->AddGrowableCol(1);
    fgs->AddGrowableRow(0);
    fgs->AddGrowableRow(3);
    fgs->AddGrowableRow(4);
    fgs->AddGrowableRow(5);

    CreateStatusBar(1);
    SetStatusText("Ready");

    this->SetSizer(fgs);
    this->Fit();
}

void MainFrame::OnCopyDeclButtonClicked(wxCommandEvent& event) {
    if (wxTheClipboard->Open())
    {
        wxTheClipboard->SetData(new wxTextDataObject(declarationsOutBox_->GetValue()));
        wxTheClipboard->Flush();
        wxTheClipboard->Close();
    }
}

void MainFrame::OnCopyAdjButtonClicked(wxCommandEvent& event) {
    if (wxTheClipboard->Open())
    {
        wxTheClipboard->SetData(new wxTextDataObject(adjointsOutBox_->GetValue()));
        wxTheClipboard->Flush();
        wxTheClipboard->Close();
    }
}

void MainFrame::OnCopyTangButtonClicked(wxCommandEvent& event) {
    if (wxTheClipboard->Open())
    {
        wxTheClipboard->SetData(new wxTextDataObject(tangentOutBox_->GetValue()));
        wxTheClipboard->Flush();
        wxTheClipboard->Close();
    }
}

void MainFrame::OnStartButtonPressed(wxCommandEvent& event){

    
    declarationsOutBox_->SetValue(wxEmptyString);
    adjointsOutBox_->SetValue(wxEmptyString);
    tangentOutBox_->SetValue(wxEmptyString);
    

    SetStatusText("Parsing Input Equation...");
    std::string input = inputAssignmentBox_->GetValue().ToStdString();              //<- user input
    int inputDerivativeOrder = condContrPan_->getDerivativeOrder();               //<- user input
    
    std::vector<std::shared_ptr<ExpAssign>> assignments;
    
    try {
        assignments = parseInputAssignments(input, inputDerivativeOrder);
    }
    catch (const EigenADException& e) {
        SetStatusText("Error");
        wxMessageBox(e.getDisplayExceptionErrorMessage(), e.getDisplayExceptionName(), wxOK | wxICON_ERROR);
    }
    catch (const antlr4::IllegalArgumentException& e) {
        SetStatusText("Error");
        wxMessageBox(e.what(), "Illegal Input Symbol", wxOK | wxICON_ERROR);
    }
    

    try {
        for (std::shared_ptr<ExpAssign>& exp : assignments) {
            differentiateAndValidate(exp);
            //only first assignment officially supported, see ExpBuilder::visitAssignments implementation
        }
    }
    catch (const EigenADException& e) {
        SetStatusText("Error");
        wxMessageBox(e.getDisplayExceptionErrorMessage(), e.getDisplayExceptionName(), wxOK | wxICON_ERROR);
    }
    
    SetStatusText("Ready");
    
}

void MainFrame::differentiateAndValidate(const std::shared_ptr<ExpAssign>& exp) {

    SetStatusText("Checking Semantics...");
    exp->checkSemantics(); //<-can throw EigenADException
    
    SetStatusText("Generating Matrix Dimensions...");
    DimGraph graph;
    
    exp->setDimensionNodes(graph);
    graph.generateMatrixDimensions();

    SetStatusText("Differentiating...");
    std::vector<std::shared_ptr<ExpAssign>> allAdjoints;
    exp->adjDifferentiate(nullptr, allAdjoints);
    std::shared_ptr<ExpAssign> tangD = exp->startTangDifferentiate();

    SetStatusText("Generating And Assigning Matrix Values...");
    MatrixCache matrixValuesCache;
    
    exp->generateMatrixValues(matrixValuesCache);

    for (std::shared_ptr<ExpAssign>& adj : allAdjoints) {
        adj->generateMatrixValues(matrixValuesCache);
    }
    
    tangD->generateMatrixValues(matrixValuesCache);
   

    SetStatusText("Calculating...");
    
    MatrixType primalResult = std::get<MatrixType>(exp->calculate()); //<- can throw EigenADException, saves primal result
    matrixValuesCache.addOutputEntry(exp->getLeft());

    std::vector<MatrixType> adjointResults;
    for (std::shared_ptr<ExpAssign>& adj : allAdjoints) {
        adjointResults.push_back(std::get<MatrixType>(adj->calculate())); //<- can throw EigenADException, saves every adjoint result
        matrixValuesCache.addOutputEntry(adj->getLeft());
    }

    MatrixType tangentResult = std::get<MatrixType>(tangD->calculate()); //<- can throw EigenADException, saves tangent result
    matrixValuesCache.addOutputEntry(tangD->getLeft());

    if (condContrPan_->getDualityValidationCheckBoxValue()) {
        SetStatusText("Validating Duality...");
        matrixValuesCache.validateDuality(); //<- can throw EigenADException
    }
    
    std::string matrixCacheDeclarations = matrixValuesCache.getMatrixDeclarations(); //get the string here, because matrixCache is modified if FDA are run

    if (condContrPan_->getForwardFDACheckBoxValue() || condContrPan_->getBackwardFDACheckBoxValue() || condContrPan_->getCentralFDACheckBoxValue()) {
        
        std::vector<std::shared_ptr<ExpAssign>> nestedSubexpressions;
        std::shared_ptr<ExpAssign> substitutedMainExp = exp->deepCopyAssignment();
        substitutedMainExp->devideIntoSubexpressions(nestedSubexpressions, matrixValuesCache);
        nestedSubexpressions.push_back(substitutedMainExp);
        //nestedSubexpressions now holds all subexpressions

        std::vector<MatrixType> nestedResults;
        std::vector<MatrixType> nestedTangentResults;
        std::vector<std::shared_ptr<ExpAssign>> nestedSubExpressionsModified;
        for (auto& expression : nestedSubexpressions) {
            MatrixType nestedRes = std::get<MatrixType>(expression->calculate());
            nestedResults.push_back(nestedRes);

            std::shared_ptr<ExpAssign> tangent = expression->startTangDifferentiate();
            tangent->generateMatrixValues(matrixValuesCache);
            MatrixType nestedResTang = std::get<MatrixType>(tangent->calculate());
            matrixValuesCache.addOutputEntry(tangent->getLeft());
            nestedTangentResults.push_back(nestedResTang);

            nestedSubExpressionsModified.push_back(expression->startTransformExpCholeskToExpLowUpp());

        }
        //nestedResults holds all primal results
        //nestedTangentResults holds all tangent results
        //nestedSubExpressionsModified now has all .lu() modified expressions


        MatrixType primalResultNested = nestedResults.at(nestedResults.size() - 1);
        if (!primalResult.isApprox(primalResultNested, EPSILON)) {
            int answer = wxMessageBox("An extracted subexpression is not equal to the original.\nContinue?", "Internal Validation Error",
                wxYES_NO | wxYES_DEFAULT | wxICON_ERROR);
            if (answer == wxNO) {
                return;
            }
        }



        if (condContrPan_->getForwardFDACheckBoxValue()) {
            SetStatusText("Validating via Forward FDA...");
            for (int i = 0; i < nestedSubExpressionsModified.size(); i++) {
                std::stringstream debug;

                nestedSubExpressionsModified.at(i)->perturbeMatrixValuesBy(FDA_AMOUNT, matrixValuesCache);
                MatrixType perturbedLuResult = std::get<MatrixType>(nestedSubExpressionsModified.at(i)->calculate());
                MatrixType approxTangent = (perturbedLuResult - nestedResults.at(i)) / FDA_AMOUNT;

                try {
                    validateMatrices(FDAType::Forward, approxTangent, nestedTangentResults.at(i));
                }
                catch(FDAValidationFailedException e) {
                    SetStatusText("Error");
                    std::stringstream results;
                    results << "Approximated Tangent:\n" << approxTangent << "\n\nActual Tangent:\n" << nestedTangentResults.at(i);
                    int answer = wxMessageBox(e.getDisplayExceptionErrorMessage() + "\n" + results.str() + "\nContinue?",
                        e.getDisplayExceptionName(),
                        wxYES_NO | wxYES_DEFAULT | wxICON_ERROR);
                    if (answer == wxNO) {
                        return;
                    }
                }
            }


        }
        if (condContrPan_->getBackwardFDACheckBoxValue()) {
            SetStatusText("Validating via Backward FDA...");
            for (int i = 0; i < nestedSubExpressionsModified.size(); i++) {
                nestedSubExpressionsModified.at(i)->perturbeMatrixValuesBy(-FDA_AMOUNT, matrixValuesCache);
                MatrixType perturbedLuResult = std::get<MatrixType>(nestedSubExpressionsModified.at(i)->calculate());
                MatrixType approxTangent = (nestedResults.at(i) - perturbedLuResult) / FDA_AMOUNT;
                try {
                    validateMatrices(FDAType::Backward, approxTangent, nestedTangentResults.at(i));
                }
                catch (FDAValidationFailedException e) {
                    SetStatusText("Error");
                    std::stringstream results;
                    results << "Approximated Tangent:\n" << approxTangent << "\n\nActual Tangent:\n" << nestedTangentResults.at(i);
                    int answer = wxMessageBox(e.getDisplayExceptionErrorMessage() + "\n" + results.str() + "\nContinue?",
                        e.getDisplayExceptionName(),
                        wxYES_NO | wxYES_DEFAULT | wxICON_ERROR);
                    if (answer == wxNO) {
                        return;
                    }
                }
            }
        }
        if (condContrPan_->getCentralFDACheckBoxValue()) {
            SetStatusText("Validating via Central FDA...");
            for (int i = 0; i < nestedSubExpressionsModified.size(); i++) {
                nestedSubExpressionsModified.at(i)->perturbeMatrixValuesBy(FDA_AMOUNT, matrixValuesCache);
                MatrixType perturbedForwardResult = std::get<MatrixType>(nestedSubExpressionsModified.at(i)->calculate());
                nestedSubExpressionsModified.at(i)->perturbeMatrixValuesBy(-FDA_AMOUNT, matrixValuesCache);
                MatrixType perturbedBackwardResult = std::get<MatrixType>(nestedSubExpressionsModified.at(i)->calculate());
                MatrixType approxTangent = (perturbedForwardResult - perturbedBackwardResult) / (2 * FDA_AMOUNT);
                try {
                    validateMatrices(FDAType::Central, approxTangent, nestedTangentResults.at(i));
                }
                catch (FDAValidationFailedException e) {
                    SetStatusText("Error");
                    std::stringstream results;
                    results << "Approximated Tangent:\n" << approxTangent << "\n\nActual Tangent:\n" << nestedTangentResults.at(i);
                    int answer = wxMessageBox(e.getDisplayExceptionErrorMessage() + "\n" + results.str() + "\nContinue?",
                        e.getDisplayExceptionName(),
                        wxYES_NO | wxYES_DEFAULT | wxICON_ERROR);
                    if (answer == wxNO) {
                        return;
                    }
                }
            }
            
        }

    }



    SetStatusText("Outputting Results...");
    declarationsOutBox_->SetValue(declarationsOutBox_->GetValue() + graph.getDimensionDeclarations() + matrixCacheDeclarations);

    std::string adjoints;
    std::string aliasedAdj;
    for (std::shared_ptr<ExpAssign>& adj : allAdjoints) {
        if (adj->getLeft()->getIsAliased()) {
            aliasedAdj = adj->toString() + "\n";
        }
        else {
            adjoints += adj->toString() + "\n";
        }    
    }
    adjoints += aliasedAdj;
    adjointsOutBox_->SetValue(adjointsOutBox_->GetValue() + adjoints);

    tangentOutBox_->SetValue(tangentOutBox_->GetValue() + tangD->toString() + "\n");
    
}

std::vector<std::shared_ptr<ExpAssign>> MainFrame::parseInputAssignments(const std::string& input, int inputDerivativeOrder){
    
    std::vector<std::shared_ptr<ExpAssign>> result;
    
    
    antlr4::ANTLRInputStream inputStream(input);    //<- can throw antlr4::IllegalArgumentException
    
    EigenExpLexer lexer(&inputStream);              //<- wxWidgets detects memory leaks when <wx/wx.h> is included (false positives -> input cache(?) only increases with a new input (https://github.com/antlr/antlr4/issues/2182) and only in debug mode)
    antlr4::CommonTokenStream tokenStream(&lexer);
    EigenExpParser parser(&tokenStream);
    ExpErrorListener errListener;
    lexer.removeErrorListeners();
    lexer.addErrorListener(&errListener);
    parser.removeErrorListeners();
    parser.addErrorListener(&errListener);
    antlr4::tree::ParseTree* tree = parser.assigns();

    if (errListener.getHasError()) {
        throw ListenerHasErrorException(errListener.getErrorMsgs());
    }
    
    ExpBuilder visitor;
    result = std::any_cast<std::vector<std::shared_ptr<ExpAssign>>>(visitor.visit(tree));

    if (result.empty()) {
        throw NoAssignmentRecognizedException();
    }

    if (inputDerivativeOrder <= visitor.getHighestOrder().second) {
        throw InvalidDerivativeOrderException(visitor.getHighestOrder(), inputDerivativeOrder);
    }
    

    Expression::setDerivativeOrder(inputDerivativeOrder);
    Expression::setOptimizeOutput(condContrPan_->getOptimizedOutputCheckBoxValue());

    //For Debugging to skip the Lexer/Parser
    /* 
    std::shared_ptr<ExpMatrix> matrixC = std::make_shared<ExpMatrix>("C", "", false);
    std::shared_ptr<ExpMatrix> matrixA = std::make_shared<ExpMatrix>("A", "", false);
    std::shared_ptr<ExpMatrix> matrixB = std::make_shared<ExpMatrix>("B", "", false);
    std::shared_ptr<ExpAdd> add = std::make_shared<ExpAdd>(matrixA, matrixB);
    std::shared_ptr<ExpAssign> assign = std::make_shared<ExpAssign>(matrixC, add); //C=A+B
    std::shared_ptr<ExpAssign> aequalsb = std::make_shared<ExpAssign>(matrixA, matrixB); //A=B
    result.push_back(assign);
    */

    /*
    std::shared_ptr<ExpMatrix> matrixA = std::make_shared<ExpMatrix>("A", "", false);
    std::shared_ptr<ExpMatrix> matrixB = std::make_shared<ExpMatrix>("B", "", false);
    std::shared_ptr<ExpMatrix> matrixC = std::make_shared<ExpMatrix>("C", "", false);
    std::shared_ptr<ExpMatrix> matrixD = std::make_shared<ExpMatrix>("D", "", false);
    std::shared_ptr<ExpMatrix> matrixE = std::make_shared<ExpMatrix>("E", "", false);
    std::shared_ptr<ExpMatrix> matrixF = std::make_shared<ExpMatrix>("F", "", false);
    std::shared_ptr<ExpMatrix> matrixG = std::make_shared<ExpMatrix>("G", "", false);
    std::shared_ptr<ExpMatrix> matrixH = std::make_shared<ExpMatrix>("H", "", false);
    std::shared_ptr<ExpMatrix> matrixI = std::make_shared<ExpMatrix>("I", "", false);
    std::shared_ptr<ExpMult> multBCD = std::make_shared<ExpMult>(matrixB, matrixC);
    std::shared_ptr<ExpMult> multBCDE = std::make_shared<ExpMult>(multBCD, matrixD);
    std::shared_ptr<ExpMult> multBCDEF = std::make_shared<ExpMult>(multBCDE, matrixE);
    std::shared_ptr<ExpMult> multBCDEFG = std::make_shared<ExpMult>(multBCDEF, matrixF);
    std::shared_ptr<ExpMult> multBCDEFGH = std::make_shared<ExpMult>(multBCDEFG, matrixG);
    std::shared_ptr<ExpCholesk> cholesk = std::make_shared<ExpCholesk>(multBCDEFGH, matrixI); // 
    std::shared_ptr<ExpAssign> assign = std::make_shared<ExpAssign>(matrixA, cholesk); //A = (B * C * D * E * F * G * H).llt().solve(I)
    result.push_back(assign);
    */

    return result;

}

void MainFrame::validateMatrices(FDAType type, MatrixType A, MatrixType B) const {
    double accAbsDiff = 0;
    double accPercDiff = 0;
    int countNonZeroDiffs = 0;
    for (int i = 0; i < A.rows(); i++) {
        for (int j = 0; j < A.cols(); j++) {
            double valueA = A(i, j);
            double valueB = B(i, j);
            double diff = std::abs(valueA - valueB);
            accAbsDiff += diff;

            double avgValue = (valueA + valueB) / 2.0;
            if (avgValue != 0) {
                accPercDiff += (diff / avgValue) * 100.0;
                countNonZeroDiffs++;
            }
            

        }
    }

    double avgAbsDiff = accAbsDiff / (A.rows() * A.cols());
    double avgPercDiff = countNonZeroDiffs ? (accPercDiff / countNonZeroDiffs) : 0.0;
    if(avgAbsDiff > FDA_EPSILON){
        if (avgPercDiff > FDA_EPSILON) {
            throw FDAValidationFailedException(type, avgAbsDiff, avgPercDiff);
        }
    }
}

void MainFrame::OnQuit(wxCommandEvent& event) {
    Close(true);
}

void MainFrame::OnDocumentation(wxCommandEvent& event){
    wxLaunchDefaultBrowser("https://git.rwth-aachen.de/Noah/EigenADST");
}

void MainFrame::OnAbout(wxCommandEvent& event){
    wxMessageBox("EigenADST version 1.0\n"
    "\n"
    "EigenADST is a software designed for differentiating and validating Eigen Matrix expressions.\n"
    "Developed by STCE at RWTH Aachen University.\n"
    "\n"
    "Using:\n"
    " - wxWidgets\n"
    " - ANTLR4\n"
    " - Eigen"
        , "About EigenADST", wxOK | wxICON_INFORMATION);
}
