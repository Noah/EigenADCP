#include "GUI/innerPanels/ConditionControlPanel.hpp"

#include <wx/panel.h>
#include <wx/spinctrl.h>
#include <wx/button.h>
#include <wx/sizer.h>

#include "GUI/ButtonID.hpp"

ConditionControlPanel::ConditionControlPanel(wxWindow* parent) : wxPanel(parent) {
    inputDerivBox_ = new wxSpinCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 100, 1);
    startButton_ = new wxButton(this, START_BUTTON, "Differentiate");

    enableOptimizedOutput_ = new wxCheckBox(this, wxID_ANY, "Optimized Output");
    enableOptimizedOutput_->SetValue(true);
    enableDualityValidation_ = new wxCheckBox(this, wxID_ANY, "Duality Validation");
    enableDualityValidation_->SetValue(true);
    enableForwardFDAValidation_ = new wxCheckBox(this, wxID_ANY, "Forward FDA Validation");
    enableBackwardFDAValidation_ = new wxCheckBox(this, wxID_ANY, "Backward FDA Validation");
    enableCentralFDAValidation_ = new wxCheckBox(this, wxID_ANY, "Central FDA Validation");

    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);

    sizer->Add(inputDerivBox_, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    sizer->Add(startButton_, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    sizer->Add(enableOptimizedOutput_, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    sizer->AddSpacer(15);
    sizer->Add(enableDualityValidation_, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    sizer->Add(enableForwardFDAValidation_, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    sizer->Add(enableBackwardFDAValidation_, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    sizer->Add(enableCentralFDAValidation_, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

    this->SetSizer(sizer);
    this->Fit();
}


int ConditionControlPanel::getDerivativeOrder() const {
    return inputDerivBox_->GetValue();
}

bool ConditionControlPanel::getOptimizedOutputCheckBoxValue() const
{
    return enableOptimizedOutput_->GetValue();
}
bool ConditionControlPanel::getDualityValidationCheckBoxValue() const
{
    return enableDualityValidation_->GetValue();
}
bool ConditionControlPanel::getForwardFDACheckBoxValue() const
{
    return enableForwardFDAValidation_->GetValue();;
}
bool ConditionControlPanel::getBackwardFDACheckBoxValue() const
{
    return enableBackwardFDAValidation_->GetValue();
}
bool ConditionControlPanel::getCentralFDACheckBoxValue() const
{
    return enableCentralFDAValidation_->GetValue();
}