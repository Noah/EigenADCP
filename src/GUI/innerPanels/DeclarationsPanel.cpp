#include "GUI/innerPanels/DeclarationsPanel.hpp"
#include <wx/panel.h>
#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/sizer.h>

#include "GUI/ButtonID.hpp"

DeclarationsPanel::DeclarationsPanel(wxWindow* parent) : wxPanel(parent){
    declDescription_ = new wxStaticText(this, wxID_ANY, "Matrix Declarations");
    copyDeclButton_ = new wxButton(this, COPY_DECLARATIONS_BUTTON, "Copy to Clipboard");

    wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
    sizer->AddStretchSpacer(10);
    sizer->Add(declDescription_, 0, wxALIGN_CENTER | wxALL, 5);
    sizer->AddStretchSpacer(3);
    sizer->Add(copyDeclButton_, 0, wxALIGN_CENTER | wxALL, 5);
    sizer->AddStretchSpacer(10);

    this->SetSizer(sizer);
    this->Fit();

}


