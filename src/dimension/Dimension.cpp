#include "dimension/Dimension.hpp"
#include <string>
#include "EigenNS.hpp"

Dimension::Dimension() {
	id_ = numerator;
	numerator++; //to debug
}

Dimension::Dimension(int size) {
	id_ = numerator;
	size_ = size;
	numerator++;
}

Dimension::Dimension(int size, int outputNum) {
	id_ = numerator;
	size_ = size;
	outputNumeration_ = outputNum;
	numerator++;
}

std::string Dimension::toString() const {
	return DIMENSION_PREFIX + std::to_string(outputNumeration_);
}

int Dimension::getSize() const
{
	return size_;
}

void Dimension::setSize(int size)
{
	size_ = size;
}

void Dimension::setNum(int num) {
	outputNumeration_ = num;
}

int Dimension::getNum() const {
	return outputNumeration_;
}

int Dimension::numerator = 0;
