#include "dimension/DimNode.hpp"

#include <string>
#include <vector>
#include <memory>
#include "dimension/DimensionType.hpp"

#include "dimension/Dimension.hpp"

DimNode::DimNode(const std::string& hashID, DimensionType dimType) {
	hashID_ = hashID;
	dimType_ = dimType;
}

std::string DimNode::toString() const {
	std::string out = getHashID() + " " + DimTypeToString(getDimType()) + ",   connections: ";
	for (auto& node : connectedNodes_) {
		out = out + node.lock()->getHashID() + " " + DimTypeToString(node.lock()->getDimType()) + ", ";
	}
	return out;
}

void DimNode::connect(const std::shared_ptr<DimNode> node1, const std::shared_ptr<DimNode> node2) {
	
	if (!node1->isConnectedTo(node2)) {
		node1->connectedNodes_.push_back(node2);
	}
	if (!node2->isConnectedTo(node1)) {
		node2->connectedNodes_.push_back(node1);
	}
	
}

DimensionType DimNode::getDimType() const {
	return dimType_;
}

bool DimNode::isDiscovered() const {
	return discovered_;
}

void DimNode::setDiscovered() {
	discovered_ = true;
}

std::string DimNode::getHashID() const {
	return hashID_;
}

void DimNode::setDimOfConnected(const Dimension& dim){
	//set own discovered flag to true and try to set Dimension
	discovered_ = true;
	setDimension(dim);
	for (auto& connected : connectedNodes_) {
		//do the same for all connected nodes
		if (!connected.lock()->isDiscovered()) {
			connected.lock()->setDimOfConnected(dim);
		}
		
	}
}

void DimNode::setDimension(const Dimension& dim) {
}

bool DimNode::isIdenticalTo(std::shared_ptr<DimNode> node) {
	return hashID_ == node->hashID_ && dimType_ == node->getDimType();
}

bool DimNode::isConnectedTo(const std::shared_ptr<DimNode>& node) const {
	for (auto& entry : connectedNodes_) {
		if (entry.lock()->getHashID() == node->getHashID() && entry.lock()->getDimType() == node->getDimType()) {
			return true;
		}
	}
	return false;
}

std::string DimNode::getMatrixName() const {
	return matrixName_;
}
