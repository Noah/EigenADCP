grammar EigenExp;

// Lexer
MATRIX_ID	: [a-zA-Z] [a-zA-Z0-9_]* ; //The getHashID() function depends on matrices not containing the character ":"

NUMBER		: [0-9]+ ((','|'.') [0-9]+)? ;

WS         	: [ \t\r\n]+ -> skip ;

// Parser
scalarValue	: NUMBER									# ScalarValueID
			;
			
matrix		: MATRIX_ID 								# MatrixID
			;
						
assigns		: (assignment ';'*)* EOF					# Assignments
			;

assignment	: matrix '=' expression						# ExpAssign
			;

expression	: expression '.transpose()'					# ExpTrans
			| expression '.llt().solve(' expression ')'	# ExpCholesk
			| expression '*' expression					# ExpMult
			| '-' expression							# ExpNeg
			| '+' expression							# ExpPos
			| expression '+' expression					# ExpAdd
			| expression '-' expression					# ExpSub
			| '(' expression ')'						# ExpParenth
			| matrix									# ExpMatrix
			| scalarValue								# ExpScalarValue
			;