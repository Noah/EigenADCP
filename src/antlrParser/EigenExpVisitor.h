
// Generated from EigenExp.g4 by ANTLR 4.12.0

#pragma once


#include "antlr4-runtime.h"
#include "EigenExpParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by EigenExpParser.
 */
class  EigenExpVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by EigenExpParser.
   */
    virtual std::any visitScalarValueID(EigenExpParser::ScalarValueIDContext *context) = 0;

    virtual std::any visitMatrixID(EigenExpParser::MatrixIDContext *context) = 0;

    virtual std::any visitAssignments(EigenExpParser::AssignmentsContext *context) = 0;

    virtual std::any visitExpAssign(EigenExpParser::ExpAssignContext *context) = 0;

    virtual std::any visitExpParenth(EigenExpParser::ExpParenthContext *context) = 0;

    virtual std::any visitExpMatrix(EigenExpParser::ExpMatrixContext *context) = 0;

    virtual std::any visitExpCholesk(EigenExpParser::ExpCholeskContext *context) = 0;

    virtual std::any visitExpAdd(EigenExpParser::ExpAddContext *context) = 0;

    virtual std::any visitExpPos(EigenExpParser::ExpPosContext *context) = 0;

    virtual std::any visitExpScalarValue(EigenExpParser::ExpScalarValueContext *context) = 0;

    virtual std::any visitExpTrans(EigenExpParser::ExpTransContext *context) = 0;

    virtual std::any visitExpNeg(EigenExpParser::ExpNegContext *context) = 0;

    virtual std::any visitExpSub(EigenExpParser::ExpSubContext *context) = 0;

    virtual std::any visitExpMult(EigenExpParser::ExpMultContext *context) = 0;


};

