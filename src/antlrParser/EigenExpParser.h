
// Generated from EigenExp.g4 by ANTLR 4.12.0

#pragma once


#include "antlr4-runtime.h"




class  EigenExpParser : public antlr4::Parser {
public:
  enum {
    T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, 
    T__7 = 8, T__8 = 9, MATRIX_ID = 10, NUMBER = 11, WS = 12
  };

  enum {
    RuleScalarValue = 0, RuleMatrix = 1, RuleAssigns = 2, RuleAssignment = 3, 
    RuleExpression = 4
  };

  explicit EigenExpParser(antlr4::TokenStream *input);

  EigenExpParser(antlr4::TokenStream *input, const antlr4::atn::ParserATNSimulatorOptions &options);

  ~EigenExpParser() override;

  std::string getGrammarFileName() const override;

  const antlr4::atn::ATN& getATN() const override;

  const std::vector<std::string>& getRuleNames() const override;

  const antlr4::dfa::Vocabulary& getVocabulary() const override;

  antlr4::atn::SerializedATNView getSerializedATN() const override;


  class ScalarValueContext;
  class MatrixContext;
  class AssignsContext;
  class AssignmentContext;
  class ExpressionContext; 

  class  ScalarValueContext : public antlr4::ParserRuleContext {
  public:
    ScalarValueContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    ScalarValueContext() = default;
    void copyFrom(ScalarValueContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  ScalarValueIDContext : public ScalarValueContext {
  public:
    ScalarValueIDContext(ScalarValueContext *ctx);

    antlr4::tree::TerminalNode *NUMBER();

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  ScalarValueContext* scalarValue();

  class  MatrixContext : public antlr4::ParserRuleContext {
  public:
    MatrixContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    MatrixContext() = default;
    void copyFrom(MatrixContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  MatrixIDContext : public MatrixContext {
  public:
    MatrixIDContext(MatrixContext *ctx);

    antlr4::tree::TerminalNode *MATRIX_ID();

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  MatrixContext* matrix();

  class  AssignsContext : public antlr4::ParserRuleContext {
  public:
    AssignsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    AssignsContext() = default;
    void copyFrom(AssignsContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  AssignmentsContext : public AssignsContext {
  public:
    AssignmentsContext(AssignsContext *ctx);

    antlr4::tree::TerminalNode *EOF();
    std::vector<AssignmentContext *> assignment();
    AssignmentContext* assignment(size_t i);

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  AssignsContext* assigns();

  class  AssignmentContext : public antlr4::ParserRuleContext {
  public:
    AssignmentContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    AssignmentContext() = default;
    void copyFrom(AssignmentContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  ExpAssignContext : public AssignmentContext {
  public:
    ExpAssignContext(AssignmentContext *ctx);

    MatrixContext *matrix();
    ExpressionContext *expression();

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  AssignmentContext* assignment();

  class  ExpressionContext : public antlr4::ParserRuleContext {
  public:
    ExpressionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    ExpressionContext() = default;
    void copyFrom(ExpressionContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  ExpParenthContext : public ExpressionContext {
  public:
    ExpParenthContext(ExpressionContext *ctx);

    ExpressionContext *expression();

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExpMatrixContext : public ExpressionContext {
  public:
    ExpMatrixContext(ExpressionContext *ctx);

    MatrixContext *matrix();

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExpCholeskContext : public ExpressionContext {
  public:
    ExpCholeskContext(ExpressionContext *ctx);

    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExpAddContext : public ExpressionContext {
  public:
    ExpAddContext(ExpressionContext *ctx);

    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExpPosContext : public ExpressionContext {
  public:
    ExpPosContext(ExpressionContext *ctx);

    ExpressionContext *expression();

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExpScalarValueContext : public ExpressionContext {
  public:
    ExpScalarValueContext(ExpressionContext *ctx);

    ScalarValueContext *scalarValue();

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExpTransContext : public ExpressionContext {
  public:
    ExpTransContext(ExpressionContext *ctx);

    ExpressionContext *expression();

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExpNegContext : public ExpressionContext {
  public:
    ExpNegContext(ExpressionContext *ctx);

    ExpressionContext *expression();

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExpSubContext : public ExpressionContext {
  public:
    ExpSubContext(ExpressionContext *ctx);

    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExpMultContext : public ExpressionContext {
  public:
    ExpMultContext(ExpressionContext *ctx);

    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);

    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  ExpressionContext* expression();
  ExpressionContext* expression(int precedence);

  bool sempred(antlr4::RuleContext *_localctx, size_t ruleIndex, size_t predicateIndex) override;

  bool expressionSempred(ExpressionContext *_localctx, size_t predicateIndex);

  // By default the static state used to implement the parser is lazily initialized during the first
  // call to the constructor. You can call this function if you wish to initialize the static state
  // ahead of time.
  static void initialize();

private:
};

