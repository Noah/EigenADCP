
// Generated from EigenExp.g4 by ANTLR 4.12.0


#include "EigenExpLexer.h"


using namespace antlr4;



using namespace antlr4;

namespace {

struct EigenExpLexerStaticData final {
  EigenExpLexerStaticData(std::vector<std::string> ruleNames,
                          std::vector<std::string> channelNames,
                          std::vector<std::string> modeNames,
                          std::vector<std::string> literalNames,
                          std::vector<std::string> symbolicNames)
      : ruleNames(std::move(ruleNames)), channelNames(std::move(channelNames)),
        modeNames(std::move(modeNames)), literalNames(std::move(literalNames)),
        symbolicNames(std::move(symbolicNames)),
        vocabulary(this->literalNames, this->symbolicNames) {}

  EigenExpLexerStaticData(const EigenExpLexerStaticData&) = delete;
  EigenExpLexerStaticData(EigenExpLexerStaticData&&) = delete;
  EigenExpLexerStaticData& operator=(const EigenExpLexerStaticData&) = delete;
  EigenExpLexerStaticData& operator=(EigenExpLexerStaticData&&) = delete;

  std::vector<antlr4::dfa::DFA> decisionToDFA;
  antlr4::atn::PredictionContextCache sharedContextCache;
  const std::vector<std::string> ruleNames;
  const std::vector<std::string> channelNames;
  const std::vector<std::string> modeNames;
  const std::vector<std::string> literalNames;
  const std::vector<std::string> symbolicNames;
  const antlr4::dfa::Vocabulary vocabulary;
  antlr4::atn::SerializedATNView serializedATN;
  std::unique_ptr<antlr4::atn::ATN> atn;
};

::antlr4::internal::OnceFlag eigenexplexerLexerOnceFlag;
EigenExpLexerStaticData *eigenexplexerLexerStaticData = nullptr;

void eigenexplexerLexerInitialize() {
  assert(eigenexplexerLexerStaticData == nullptr);
  auto staticData = std::make_unique<EigenExpLexerStaticData>(
    std::vector<std::string>{
      "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
      "MATRIX_ID", "NUMBER", "WS"
    },
    std::vector<std::string>{
      "DEFAULT_TOKEN_CHANNEL", "HIDDEN"
    },
    std::vector<std::string>{
      "DEFAULT_MODE"
    },
    std::vector<std::string>{
      "", "';'", "'='", "'.transpose()'", "'.llt().solve('", "')'", "'*'", 
      "'-'", "'+'", "'('"
    },
    std::vector<std::string>{
      "", "", "", "", "", "", "", "", "", "", "MATRIX_ID", "NUMBER", "WS"
    }
  );
  static const int32_t serializedATNSegment[] = {
  	4,0,12,93,6,-1,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
  	6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,1,0,1,0,1,1,1,1,1,2,1,2,
  	1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,3,1,3,1,3,1,3,1,3,1,3,1,
  	3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,4,1,4,1,5,1,5,1,6,1,6,1,7,1,7,1,8,1,8,
  	1,9,1,9,5,9,69,8,9,10,9,12,9,72,9,9,1,10,4,10,75,8,10,11,10,12,10,76,
  	1,10,1,10,4,10,81,8,10,11,10,12,10,82,3,10,85,8,10,1,11,4,11,88,8,11,
  	11,11,12,11,89,1,11,1,11,0,0,12,1,1,3,2,5,3,7,4,9,5,11,6,13,7,15,8,17,
  	9,19,10,21,11,23,12,1,0,5,2,0,65,90,97,122,4,0,48,57,65,90,95,95,97,122,
  	1,0,48,57,2,0,44,44,46,46,3,0,9,10,13,13,32,32,97,0,1,1,0,0,0,0,3,1,0,
  	0,0,0,5,1,0,0,0,0,7,1,0,0,0,0,9,1,0,0,0,0,11,1,0,0,0,0,13,1,0,0,0,0,15,
  	1,0,0,0,0,17,1,0,0,0,0,19,1,0,0,0,0,21,1,0,0,0,0,23,1,0,0,0,1,25,1,0,
  	0,0,3,27,1,0,0,0,5,29,1,0,0,0,7,42,1,0,0,0,9,56,1,0,0,0,11,58,1,0,0,0,
  	13,60,1,0,0,0,15,62,1,0,0,0,17,64,1,0,0,0,19,66,1,0,0,0,21,74,1,0,0,0,
  	23,87,1,0,0,0,25,26,5,59,0,0,26,2,1,0,0,0,27,28,5,61,0,0,28,4,1,0,0,0,
  	29,30,5,46,0,0,30,31,5,116,0,0,31,32,5,114,0,0,32,33,5,97,0,0,33,34,5,
  	110,0,0,34,35,5,115,0,0,35,36,5,112,0,0,36,37,5,111,0,0,37,38,5,115,0,
  	0,38,39,5,101,0,0,39,40,5,40,0,0,40,41,5,41,0,0,41,6,1,0,0,0,42,43,5,
  	46,0,0,43,44,5,108,0,0,44,45,5,108,0,0,45,46,5,116,0,0,46,47,5,40,0,0,
  	47,48,5,41,0,0,48,49,5,46,0,0,49,50,5,115,0,0,50,51,5,111,0,0,51,52,5,
  	108,0,0,52,53,5,118,0,0,53,54,5,101,0,0,54,55,5,40,0,0,55,8,1,0,0,0,56,
  	57,5,41,0,0,57,10,1,0,0,0,58,59,5,42,0,0,59,12,1,0,0,0,60,61,5,45,0,0,
  	61,14,1,0,0,0,62,63,5,43,0,0,63,16,1,0,0,0,64,65,5,40,0,0,65,18,1,0,0,
  	0,66,70,7,0,0,0,67,69,7,1,0,0,68,67,1,0,0,0,69,72,1,0,0,0,70,68,1,0,0,
  	0,70,71,1,0,0,0,71,20,1,0,0,0,72,70,1,0,0,0,73,75,7,2,0,0,74,73,1,0,0,
  	0,75,76,1,0,0,0,76,74,1,0,0,0,76,77,1,0,0,0,77,84,1,0,0,0,78,80,7,3,0,
  	0,79,81,7,2,0,0,80,79,1,0,0,0,81,82,1,0,0,0,82,80,1,0,0,0,82,83,1,0,0,
  	0,83,85,1,0,0,0,84,78,1,0,0,0,84,85,1,0,0,0,85,22,1,0,0,0,86,88,7,4,0,
  	0,87,86,1,0,0,0,88,89,1,0,0,0,89,87,1,0,0,0,89,90,1,0,0,0,90,91,1,0,0,
  	0,91,92,6,11,0,0,92,24,1,0,0,0,6,0,70,76,82,84,89,1,6,0,0
  };
  staticData->serializedATN = antlr4::atn::SerializedATNView(serializedATNSegment, sizeof(serializedATNSegment) / sizeof(serializedATNSegment[0]));

  antlr4::atn::ATNDeserializer deserializer;
  staticData->atn = deserializer.deserialize(staticData->serializedATN);

  const size_t count = staticData->atn->getNumberOfDecisions();
  staticData->decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    staticData->decisionToDFA.emplace_back(staticData->atn->getDecisionState(i), i);
  }
  eigenexplexerLexerStaticData = staticData.release();
}

}

EigenExpLexer::EigenExpLexer(CharStream *input) : Lexer(input) {
  EigenExpLexer::initialize();
  _interpreter = new atn::LexerATNSimulator(this, *eigenexplexerLexerStaticData->atn, eigenexplexerLexerStaticData->decisionToDFA, eigenexplexerLexerStaticData->sharedContextCache);
}

EigenExpLexer::~EigenExpLexer() {
  delete _interpreter;
}

std::string EigenExpLexer::getGrammarFileName() const {
  return "EigenExp.g4";
}

const std::vector<std::string>& EigenExpLexer::getRuleNames() const {
  return eigenexplexerLexerStaticData->ruleNames;
}

const std::vector<std::string>& EigenExpLexer::getChannelNames() const {
  return eigenexplexerLexerStaticData->channelNames;
}

const std::vector<std::string>& EigenExpLexer::getModeNames() const {
  return eigenexplexerLexerStaticData->modeNames;
}

const dfa::Vocabulary& EigenExpLexer::getVocabulary() const {
  return eigenexplexerLexerStaticData->vocabulary;
}

antlr4::atn::SerializedATNView EigenExpLexer::getSerializedATN() const {
  return eigenexplexerLexerStaticData->serializedATN;
}

const atn::ATN& EigenExpLexer::getATN() const {
  return *eigenexplexerLexerStaticData->atn;
}




void EigenExpLexer::initialize() {
  ::antlr4::internal::call_once(eigenexplexerLexerOnceFlag, eigenexplexerLexerInitialize);
}
