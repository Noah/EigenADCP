
// Generated from EigenExp.g4 by ANTLR 4.12.0

#pragma once


#include "antlr4-runtime.h"
#include "EigenExpVisitor.h"


/**
 * This class provides an empty implementation of EigenExpVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  EigenExpBaseVisitor : public EigenExpVisitor {
public:

  virtual std::any visitScalarValueID(EigenExpParser::ScalarValueIDContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitMatrixID(EigenExpParser::MatrixIDContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitAssignments(EigenExpParser::AssignmentsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExpAssign(EigenExpParser::ExpAssignContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExpParenth(EigenExpParser::ExpParenthContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExpMatrix(EigenExpParser::ExpMatrixContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExpCholesk(EigenExpParser::ExpCholeskContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExpAdd(EigenExpParser::ExpAddContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExpPos(EigenExpParser::ExpPosContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExpScalarValue(EigenExpParser::ExpScalarValueContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExpTrans(EigenExpParser::ExpTransContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExpNeg(EigenExpParser::ExpNegContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExpSub(EigenExpParser::ExpSubContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExpMult(EigenExpParser::ExpMultContext *ctx) override {
    return visitChildren(ctx);
  }


};

