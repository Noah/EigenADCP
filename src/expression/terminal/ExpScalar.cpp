#include "expression/terminal/ExpScalar.hpp"

#include <memory>
#include <string>
#include <vector>
#include <cmath>
#include <variant>

#include "expression/Expression.hpp"
#include "expression/ExpressionType.hpp"
#include "values/MatrixCache.hpp"
#include "expression/ExpressionResultType.hpp"

#include "EigenNS.hpp"
#include "dimension/DimGraph.hpp"

ExpScalar::ExpScalar(ScalarValueType value) {
	scalarValue_ = value;
}

ExpScalar::ExpScalar(ScalarValueType value, ExpressionResultType resultType) {
	scalarValue_ = value;
	resultType_ = resultType;
}

std::string ExpScalar::toString() const {
	if (std::floor(scalarValue_) == scalarValue_) {
		return std::to_string(static_cast<int>(scalarValue_));
	}
	else {
		return std::to_string(scalarValue_);
	}
}

std::shared_ptr<Expression> ExpScalar::tangDifferentiate() const {
	return deepCopy();
}

void ExpScalar::adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const {
}

void ExpScalar::setDimensionNodes(DimGraph& graph) {
}


std::variant<ScalarValueType, MatrixType> ExpScalar::calculate() const {
	std::variant<ScalarValueType, MatrixType> res = scalarValue_;
	return res;
}

std::shared_ptr<Expression> ExpScalar::deepCopy() const {
	return std::make_shared<ExpScalar>(scalarValue_, resultType_);
}

ExpressionResultType ExpScalar::checkSemantics() {
	return resultType_ = ExpressionResultType::Scalar;
}

void ExpScalar::generateMatrixValues(MatrixCache& matrixValuesCache) {
}

std::vector<std::vector<ExpressionResultType>> ExpScalar::getRequiredTypes() const {
	return { {} };
}

ExpressionType ExpScalar::getExpressionType() const {
	return ExpressionType::Scalar;
}

void ExpScalar::perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) {
}
