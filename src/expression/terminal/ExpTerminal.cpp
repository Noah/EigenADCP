#include "expression/terminal/ExpTerminal.hpp"

#include <memory>
#include <string>
#include <vector>

#include "expression/Expression.hpp"
#include "expression/ExpressionResultType.hpp"
#include "expression/ExpressionType.hpp"
#include "values/MatrixCache.hpp"

void ExpTerminal::devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache) {
	
}

std::shared_ptr<Expression> ExpTerminal::transformExpCholeskToExpLowUpp() const{
	return deepCopy();
}
