#include "expression/terminal/ExpMatrix.hpp"

#include <memory>
#include <string>
#include <vector>

#include "expression/Expression.hpp"
#include "expression/ExpressionResultType.hpp"
#include "expression/ExpressionType.hpp"

#include "dimension/Dimension.hpp"
#include "values/MatrixCache.hpp"
#include "exception/EigenADException.hpp"
#include "dimension/DimensionType.hpp"
#include "dimension/DimNode.hpp"
#include "dimension/DimNodeMatrix.hpp"
#include "expression/DerivativeType.hpp"
#include "expression/ExpAssign.hpp"

#include "EigenNS.hpp"
#include "dimension/DimGraph.hpp"


ExpMatrix::ExpMatrix(std::string name, std::string suffixes, bool isAliased) {
	name_ = name;
	suffixes_ = suffixes;
	isAliased_ = isAliased;
	setAndIncrementID();
}

ExpMatrix::ExpMatrix(std::string name, std::string suffixes, DerivativeType mode, int iteration, bool isAliased) {
	name_ = name;
	suffixes_ = suffixes;
	mode_ = mode;
	iteration_ = iteration;
	isAliased_ = isAliased;
	setAndIncrementID();
}

ExpMatrix::ExpMatrix(std::string name, std::string suffixes, DerivativeType mode, int iteration, bool isAliased, bool isDiff, ExpressionResultType resultType,
	std::shared_ptr<Dimension> row, std::shared_ptr<Dimension> column, MatrixType value) {
	//deep Copy Constructor
	name_ = name;
	suffixes_ = suffixes;
	mode_ = mode;
	iteration_ = iteration;
	isAliased_ = isAliased;
	isDiff_ = isDiff;
	resultType_ = resultType;
	row_ = row;
	column_ = column;
	matrixValues_ = value;
	setAndIncrementID();
}

std::string ExpMatrix::toString() const {
	return (mode_ != DerivativeType::Primal) ? (name_ + suffixes_ + "_" + DerivTypeToString(mode_) + std::to_string(iteration_)) : name_ + suffixes_;
}

std::shared_ptr<Expression> ExpMatrix::tangDifferentiate() const{
	return std::make_shared<ExpMatrix>(name_, suffixes_, DerivativeType::Tangent, derivativeOrder, isAliased_, true, resultType_, row_, column_, matrixValues_);
}

void ExpMatrix::adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const {
	std::shared_ptr<ExpMatrix> potNewAdjMatrix = std::make_shared<ExpMatrix>(name_, suffixes_, DerivativeType::Adjoint, derivativeOrder, isAliased_, true, resultType_, row_, column_, matrixValues_);
	for (auto& adj : allAdjoints) {
		if (potNewAdjMatrix->isEqualTo( adj->getLeft() )) {
			adj = adj->accumulateAdjoint(tempAdj);

			return;
		}
	}

	std::shared_ptr<ExpAssign> adjointDiff = std::make_shared<ExpAssign>(potNewAdjMatrix, tempAdj, DerivativeType::Adjoint, ExpressionResultType::Matrix);
	allAdjoints.push_back(adjointDiff);
}

void ExpMatrix::setDimensionNodes(DimGraph& graph) {
}

std::variant<ScalarValueType, MatrixType> ExpMatrix::calculate() const {
	if (matrixValues_.rows() == 0 || matrixValues_.cols() == 0) {
		throw UninitializedValuesException(getExpressionType(), toString(), "calculate()");
	}
	else {
		return matrixValues_;
	}
	
}

std::string ExpMatrix::getHashID() const { 
	return ExpressionTypeToString(getExpressionType()) + ":" + std::to_string(id_) + ":" + name_ + suffixes_ + "_" + DerivTypeToString(mode_) + std::to_string(iteration_);
}

void ExpMatrix::generateMatrixValues(MatrixCache& matrixValuesCache) {
	if (matrixValuesCache.containsKey(toString())) {
		MatrixType foundValues = matrixValuesCache.getValuesOf(toString());
		if (foundValues.rows() == row_->getSize() && foundValues.cols() == column_->getSize()) {
			matrixValues_ = matrixValuesCache.getValuesOf(toString());
			//std::cout << toString() << " gets " << matrixValuesCache.getValuesOf(toString()) << std::endl;
		}
		else {
			throw InvalidDimensionsException(toString(), foundValues.rows(), foundValues.cols(), row_->getSize(), column_->getSize());
		}
		
	}
	else {
		matrixValues_ = MatrixType::Random(row_->getSize(), column_->getSize());
		matrixValuesCache.addInputEntry(toString(), name_, suffixes_, iteration_, mode_, matrixValues_, row_->toString(), column_->toString());
		//std::cout << toString() << " generated " << matrixValues_ << std::endl;
	}
}

bool ExpMatrix::isEqualTo(const std::shared_ptr<ExpMatrix> mat) {
	return name_ == mat->getName() && suffixes_ == mat->getSuffixes() && mode_ == mat->getMode() && iteration_ == mat->getIteration();
}

bool ExpMatrix::isDiff() const {
	return isDiff_;
}

std::string ExpMatrix::getName() const{
	return name_;
}

std::string ExpMatrix::getSuffixes() const{
	return suffixes_;
}

DerivativeType ExpMatrix::getMode() const {
	return mode_;
}

int ExpMatrix::getIteration() const{
	return iteration_;
}

std::shared_ptr<Dimension> ExpMatrix::getRowDim() {
	return row_;
}

std::shared_ptr<Dimension> ExpMatrix::getColDim() {
	return column_;
}

MatrixType ExpMatrix::getMatrixValues() const {
	return matrixValues_;
}

std::shared_ptr<DimNode> ExpMatrix::createRowDimNode() const {
	return std::make_shared<DimNodeMatrix>(getHashID(), name_, DimensionType::Row, row_);
}

std::shared_ptr<DimNode> ExpMatrix::createColDimNode() const {
	return std::make_shared<DimNodeMatrix>(getHashID(), name_, DimensionType::Column, column_);
}

std::shared_ptr<Expression> ExpMatrix::deepCopy() const {
	return std::make_shared<ExpMatrix>(name_, suffixes_, mode_, iteration_, isAliased_, isDiff_, resultType_, row_, column_, matrixValues_);
}

std::shared_ptr<ExpMatrix> ExpMatrix::deepCopyMatrix() const {
	return std::make_shared<ExpMatrix>(name_, suffixes_, mode_, iteration_, isAliased_, isDiff_, resultType_, row_, column_, matrixValues_);
}

ExpressionResultType ExpMatrix::checkSemantics() {
	return resultType_ = ExpressionResultType::Matrix;
}

void ExpMatrix::setMatrixValues(MatrixType matrixValues){
	matrixValues_ = matrixValues;
}

void ExpMatrix::initialiseTangentMatrix() {
	mode_ = DerivativeType::Tangent;
	iteration_ = derivativeOrder;
	isDiff_ = true;
}

bool ExpMatrix::getIsAliased() const {
	return isAliased_;
}

void ExpMatrix::initialiseAdjointMatrix() {
	mode_ = DerivativeType::Adjoint;
	iteration_ = derivativeOrder;
	isDiff_ = true;
}

std::vector<std::vector<ExpressionResultType>> ExpMatrix::getRequiredTypes() const {
	return { {} };
}

ExpressionType ExpMatrix::getExpressionType() const {
	return ExpressionType::Matrix;
}

void ExpMatrix::perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) {
	if (matrixValues_.rows() == 0 || matrixValues_.cols() == 0) {
		throw UninitializedValuesException(getExpressionType(), toString(), "perturbeMatrixValuesBy()");
	}
	else {
		//get original matrix value of this matrix and counterPartMatrix and save perturbed
		//TODO what if getValues returns nothing
		
		if (matrixValueCache.containsKey(toString())) {
			matrixValues_ = matrixValueCache.getValuesOf(toString());
			matrixValues_ = matrixValues_ + amount * matrixValueCache.findCounterPartValueTo(name_, suffixes_, iteration_);
		}
		

	}
}
