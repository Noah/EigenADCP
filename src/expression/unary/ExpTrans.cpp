#include "expression/unary/ExpTrans.hpp"

#include <memory>
#include <string>
#include <vector>

#include "expression/Expression.hpp"
#include "expression/ExpressionType.hpp"
#include "expression/ExpressionResultType.hpp"
#include "values/MatrixCache.hpp"

#include "dimension/DimGraph.hpp"
#include "exception/EigenADException.hpp"


std::string ExpTrans::toString() const {
	return "( " + exp_->toString() + " ).transpose()";
}

std::shared_ptr<Expression> ExpTrans::tangDifferentiate() const{
	ExpressionResultType expRT = exp_->getExpressionResultType();
	if (expRT == ExpressionResultType::Matrix) {
		return std::make_shared<ExpTrans>(exp_->tangDifferentiate(), resultType_);
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "tangDifferentiate(...)");
	}
}

void ExpTrans::adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const {
	ExpressionResultType expRT = exp_->getExpressionResultType();
	if (expRT == ExpressionResultType::Matrix) {
		std::shared_ptr<ExpTrans> adjForExp = std::make_shared<ExpTrans>(tempAdj, resultType_);
		exp_->adjDifferentiate(adjForExp, allAdjoints);
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "adjDifferentiate(...)");
	}
	
}

void ExpTrans::setDimensionNodes(DimGraph& graph) {
	ExpressionResultType expRT = exp_->getExpressionResultType();
	if (expRT == ExpressionResultType::Matrix) {
		graph.connectToAndAddIfAbsent(exp_->createRowDimNode(), getHashID(), DimensionType::Column);
		graph.connectToAndAddIfAbsent(exp_->createColDimNode(), getHashID(), DimensionType::Row);

		exp_->setDimensionNodes(graph);
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "setDimensionNodes(...)");
	}
}


std::variant<ScalarValueType, MatrixType> ExpTrans::calculate() const {

	ExpressionResultType expRT = exp_->getExpressionResultType();

	if (expRT == ExpressionResultType::Matrix) {
		return (std::get<MatrixType>(exp_->calculate())).transpose();
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "calculate()");
	}
}

std::shared_ptr<Expression> ExpTrans::deepCopy() const {
	return std::make_shared<ExpTrans>(exp_->deepCopy(), resultType_);
}

ExpressionResultType ExpTrans::checkSemantics() {

	ExpressionResultType expRT = exp_->checkSemantics();

	if (expRT == ExpressionResultType::Matrix) {
		//Can only transpose a matrix
		return resultType_ = ExpressionResultType::Matrix;
	}
	else {
		throw InvalidMatrixExpressionException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "checkSemantics()");
	}
}

std::vector<std::vector<ExpressionResultType>> ExpTrans::getRequiredTypes() const {
	return { {ExpressionResultType::Matrix} };
}

ExpressionType ExpTrans::getExpressionType() const {
	return ExpressionType::Transposition;
}

std::shared_ptr<Expression> ExpTrans::transformExpCholeskToExpLowUpp() const {
	return std::make_shared<ExpTrans>(exp_->transformExpCholeskToExpLowUpp(), resultType_);

}
