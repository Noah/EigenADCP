#include "expression/unary/ExpNeg.hpp"
#include <memory>
#include <string>
#include <vector>

#include "expression/Expression.hpp"
#include "expression/ExpressionType.hpp"
#include "expression/ExpressionResultType.hpp"
#include "values/MatrixCache.hpp"
#include "exception/EigenADException.hpp"
#include "dimension/DimGraph.hpp"


std::string ExpNeg::toString() const {
	return "(-" + exp_->toString() + ")";
}

std::shared_ptr<Expression> ExpNeg::tangDifferentiate() const{
	ExpressionResultType expRT = exp_->getExpressionResultType();
	if (expRT == ExpressionResultType::Matrix) {
		return std::make_unique<ExpNeg>(exp_->tangDifferentiate(), resultType_);
	}
	else if (expRT == ExpressionResultType::Scalar) {
		return deepCopy();
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "tangDifferentiate()");
	}
}

void ExpNeg::adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const {
	ExpressionResultType expRT = exp_->getExpressionResultType();
	if (expRT == ExpressionResultType::Matrix) {
		std::shared_ptr<Expression> adjForExp = std::make_shared<ExpNeg>(tempAdj, resultType_);
		exp_->adjDifferentiate(adjForExp, allAdjoints);
	}
	else if (expRT == ExpressionResultType::Scalar) {

	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "adjDifferentiate(...)");
	}
}

void ExpNeg::setDimensionNodes(DimGraph& graph) {
	ExpressionResultType expRT = exp_->getExpressionResultType();
	if (expRT == ExpressionResultType::Matrix) {

		graph.connectToAndAddIfAbsent(exp_->createRowDimNode(), getHashID(), DimensionType::Row);
		graph.connectToAndAddIfAbsent(exp_->createColDimNode(), getHashID(), DimensionType::Column);
		exp_->setDimensionNodes(graph);
	}
	else if (expRT == ExpressionResultType::Scalar) {

	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "setDimensionNodes(...)");
	}
}

std::shared_ptr<Expression> ExpNeg::deepCopy() const {
	return std::make_shared<ExpNeg>(exp_->deepCopy(), resultType_);
}

std::variant<ScalarValueType, MatrixType> ExpNeg::calculate() const {
	ExpressionResultType expRT = exp_->getExpressionResultType();
	std::variant<ScalarValueType, MatrixType> res;
	if (expRT == ExpressionResultType::Matrix) {
		res = -1 * std::get<MatrixType>(exp_->calculate());
		return res;
	}
	else if (expRT == ExpressionResultType::Scalar) {
		//Scalar
		res = -1 * std::get<ScalarValueType>(exp_->calculate());
		return res;
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "calculate()");
	}
}

ExpressionResultType ExpNeg::checkSemantics() {
	ExpressionResultType expRT = exp_->checkSemantics();
	if (expRT == ExpressionResultType::Matrix || expRT == ExpressionResultType::Scalar) {
		return resultType_ = expRT;
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "checkSemantics()");
	}
}

std::vector<std::vector<ExpressionResultType>> ExpNeg::getRequiredTypes() const {
	return { {ExpressionResultType::Matrix}, {ExpressionResultType::Scalar} };
}

ExpressionType ExpNeg::getExpressionType() const {
	return ExpressionType::Negation;
}

std::shared_ptr<Expression> ExpNeg::transformExpCholeskToExpLowUpp() const {
	return std::make_shared<ExpNeg>(exp_->transformExpCholeskToExpLowUpp(), resultType_);
}