#include "expression/unary/ExpParenth.hpp"
#include <memory>
#include <string>
#include <vector>

#include "expression/Expression.hpp"
#include "expression/ExpressionResultType.hpp"
#include "expression/ExpressionType.hpp"
#include "values/MatrixCache.hpp"
#include "dimension/DimGraph.hpp"
#include "exception/EigenADException.hpp"

std::string ExpParenth::toString() const {
	if (exp_->getExpressionType() == ExpressionType::Parenthesis) {
		return exp_->toString();
	}
	return "(" + exp_->toString() + ")";
}

std::shared_ptr<Expression> ExpParenth::tangDifferentiate() const{
	ExpressionResultType expRT = exp_->getExpressionResultType();
	if (expRT == ExpressionResultType::Matrix) {

		return std::make_unique<ExpParenth>(exp_->tangDifferentiate(), resultType_);
	}
	else if(expRT == ExpressionResultType::Scalar){
		return deepCopy();
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "tangDifferentiate()");
	}

	
}

void ExpParenth::adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const {
	ExpressionResultType expRT = exp_->getExpressionResultType();
	if (expRT == ExpressionResultType::Matrix) {
		std::shared_ptr<Expression> adjForExp = std::make_shared<ExpParenth>(tempAdj, resultType_);
		exp_->adjDifferentiate(adjForExp, allAdjoints);
	}
	else if (expRT == ExpressionResultType::Scalar) {

	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "adjDifferentiate(...)");
	}
}

void ExpParenth::setDimensionNodes(DimGraph& graph) {
	ExpressionResultType expRT = exp_->getExpressionResultType();
	if (expRT == ExpressionResultType::Matrix) {
		graph.connectToAndAddIfAbsent(exp_->createRowDimNode(), getHashID(), DimensionType::Row);
		graph.connectToAndAddIfAbsent(exp_->createColDimNode(), getHashID(), DimensionType::Column);
		exp_->setDimensionNodes(graph);
	}
	else if (expRT == ExpressionResultType::Scalar) {

	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "setDimensionNodes(...)");
	}
}

std::variant<ScalarValueType, MatrixType> ExpParenth::calculate() const {
	ExpressionResultType expRT = exp_->getExpressionResultType();
	if (expRT == ExpressionResultType::Matrix || expRT == ExpressionResultType::Scalar) {
		return exp_->calculate();
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "calculate()");
	}
}

std::shared_ptr<Expression> ExpParenth::deepCopy() const {
	return std::make_shared<ExpParenth>(exp_->deepCopy(), resultType_);
}

ExpressionResultType ExpParenth::checkSemantics() {
	ExpressionResultType expRT = exp_->checkSemantics();
	if (expRT == ExpressionResultType::Matrix || expRT == ExpressionResultType::Scalar) {
		return resultType_ = expRT;
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { expRT }, getRequiredTypes(), toString(), "checkSemantics()");
	}
	
}

std::vector<std::vector<ExpressionResultType>> ExpParenth::getRequiredTypes() const {
	return { {ExpressionResultType::Matrix}, {ExpressionResultType::Scalar} };
}

ExpressionType ExpParenth::getExpressionType() const {
	return ExpressionType::Parenthesis;
}

std::shared_ptr<Expression> ExpParenth::transformExpCholeskToExpLowUpp() const {
	return std::make_shared<ExpParenth>(exp_->transformExpCholeskToExpLowUpp(), resultType_);
}
