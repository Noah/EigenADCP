#include "expression/Expression.hpp"

#include <string>
#include <memory>
#include <vector>

#include "expression/ExpressionResultType.hpp"
#include "dimension/DimensionType.hpp"
#include "dimension/DimNode.hpp"
#include "values/MatrixCache.hpp"


int Expression::iteratID = 0;
int Expression::derivativeOrder = 0;
bool Expression::optimizeOutput = true;

void Expression::setDerivativeOrder(int ord) {
    derivativeOrder = ord;
}

int Expression::getDerivativeOrder() {
    return derivativeOrder;
}


void Expression::setOptimizeOutput(bool value) {
    optimizeOutput = value;
}

bool Expression::getOptimizeOutput() {
    return optimizeOutput;
}

ExpressionResultType Expression::getExpressionResultType() const {
    return resultType_;
}

void Expression::setReturnType(ExpressionResultType retType) {
    resultType_ = retType;
}

void Expression::setAndIncrementID() {
    iteratID++;
    id_ = iteratID;
}

int Expression::getID() const{
    return id_;
}

std::string Expression::getHashID() const {
    return ExpressionTypeToString(getExpressionType()) + ":" + std::to_string(id_);
}


std::shared_ptr<DimNode> Expression::createRowDimNode() const {
    return std::make_shared<DimNode>(getHashID(), DimensionType::Row);
}

std::shared_ptr<DimNode> Expression::createColDimNode() const {
    return std::make_shared<DimNode>(getHashID(), DimensionType::Column);
}