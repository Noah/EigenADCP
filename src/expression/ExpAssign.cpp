#include "expression/ExpAssign.hpp"
#include <memory>
#include <string>
#include <vector>

#include "expression/Expression.hpp"
#include "expression/binary/ExpAdd.hpp"
#include "expression/ExpressionResultType.hpp"
#include "expression/ExpressionType.hpp"
#include "exception/EigenADException.hpp"
#include "expression/terminal/ExpMatrix.hpp"
#include "dimension/DimGraph.hpp"
#include "EigenNS.hpp"
#include "values/MatrixCache.hpp"
#include <dimension/DimNodeMatrix.hpp>
#include "expression/DerivativeType.hpp"

ExpAssign::ExpAssign(std::shared_ptr<ExpMatrix> left, std::shared_ptr<Expression> right) {
	left_ = left;
	right_ = right;
	setAndIncrementID();
}

ExpAssign::ExpAssign(std::shared_ptr<ExpMatrix> left, std::shared_ptr<Expression> right, DerivativeType derType, ExpressionResultType resultType) {
	left_ = left;
	right_ = right;
	derType_ = derType;
	resultType_ = resultType;
	setAndIncrementID();
}

std::string ExpAssign::toString() const {

	if (derType_ == DerivativeType::Adjoint && !left_->getIsAliased()) {
		return left_->toString() + " += " + right_->toString() + ";";
	}
	return left_->toString() + " = " + right_->toString() + ";";

}

std::shared_ptr<ExpAssign> ExpAssign::startTangDifferentiate() const {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		std::shared_ptr<ExpMatrix> newLeft = left_->deepCopyMatrix();
		newLeft->initialiseTangentMatrix();
		std::shared_ptr<ExpMatrix> initialPrimalRes = left_->deepCopyMatrix();
		return std::make_shared<ExpAssign>(newLeft, right_->tangDifferentiate(), DerivativeType::Tangent, resultType_);
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "tangDifferentiate(...)");
	}
}

std::shared_ptr<Expression> ExpAssign::tangDifferentiate() const{
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		std::shared_ptr<ExpMatrix> newLeft = left_->deepCopyMatrix();
		newLeft->initialiseTangentMatrix();
		std::shared_ptr<ExpMatrix> initialPrimalRes = left_->deepCopyMatrix();
		return std::make_shared<ExpAssign>(newLeft, right_->tangDifferentiate(), DerivativeType::Tangent, resultType_);
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "tangDifferentiate(...)");
	}
}

void ExpAssign::adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		std::shared_ptr<ExpMatrix> adjMatrix = left_->deepCopyMatrix();
		adjMatrix->initialiseAdjointMatrix();
		std::shared_ptr<ExpMatrix> initialPrimalRes = left_->deepCopyMatrix();
		right_->adjDifferentiate(adjMatrix, allAdjoints);
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "adjDifferentiate(...)");
	}
	
}

std::shared_ptr<Expression> ExpAssign::deepCopy() const {
	std::shared_ptr<ExpMatrix> lCopy = left_->deepCopyMatrix();
	std::shared_ptr<Expression> rCopy = right_->deepCopy();
	return std::make_shared<ExpAssign>(lCopy, rCopy, derType_, resultType_);
}

void ExpAssign::setDimensionNodes(DimGraph& graph) {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();
	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
	//modifies toString(): left matrix does not have to have the same dimensions as its occurence in the right part
	//but its dimensions still need to be inferred based on what the right side yields in order to generate the left sides adjoint
		graph.addIfAbsentAndConnect(std::make_shared<DimNodeMatrix>(left_->getHashID(), "/unique:" + left_->getName(),
			DimensionType::Row, left_->getRowDim()),
			right_->createRowDimNode());
		graph.addIfAbsentAndConnect(std::make_shared<DimNodeMatrix>(left_->getHashID(), "/unique:" + left_->getName(),
			DimensionType::Column, left_->getColDim()),
			right_->createColDimNode());

		right_->setDimensionNodes(graph);
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "setDimensionNodes(...)");
	}
}


std::variant<ScalarValueType, MatrixType> ExpAssign::calculate() const {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();
	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		std::variant<ScalarValueType, MatrixType> res = right_->calculate();
		left_->setMatrixValues(std::get<MatrixType>(res));
		return res;
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "calculate()");
	}
	
}

ExpressionResultType ExpAssign::checkSemantics() {
	ExpressionResultType leftRT = left_->checkSemantics();
	ExpressionResultType rightRT = right_->checkSemantics();

	if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		return resultType_ = ExpressionResultType::Scalar;
	}
	else if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		return resultType_ = ExpressionResultType::Matrix;
	}
	else {
		throw InvalidMatrixExpressionException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "checkSemantics()");
	}
}

void ExpAssign::generateMatrixValues(MatrixCache& matrixValuesCache) {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();
	if (rightRT == ExpressionResultType::Matrix) {
		right_->generateMatrixValues(matrixValuesCache);
	}
	else {
		throw InvalidMatrixExpressionException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString() , "generateMatrixValues(...)");
	}

}

std::shared_ptr<ExpAssign> ExpAssign::accumulateAdjoint(const std::shared_ptr<Expression>& adjoint) const {
	std::shared_ptr<ExpAdd> add = std::make_shared<ExpAdd>(right_, adjoint, ExpressionResultType::Matrix);
	return std::make_shared<ExpAssign>(left_, add, DerivativeType::Adjoint, resultType_);
}

std::shared_ptr<ExpMatrix> ExpAssign::getLeft() const {
	return left_;
}

std::shared_ptr<Expression> ExpAssign::getRight() const {
	return right_;
}

std::vector<std::vector<ExpressionResultType>> ExpAssign::getRequiredTypes() const {
	return { {ExpressionResultType::Matrix, ExpressionResultType::Matrix} };
}

ExpressionType ExpAssign::getExpressionType() const {
	return ExpressionType::Assignment;
}

void ExpAssign::perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache){
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();
	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		right_->perturbeMatrixValuesBy(amount, matrixValueCache);
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "calculate()");
	}

}

void ExpAssign::devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache) {
	left_ = std::make_shared<ExpMatrix>("LLTSubstitutionMatrix:" + std::to_string(id_), "", DerivativeType::Primal, 0, false, false, ExpressionResultType::Matrix, left_->getRowDim(), left_->getColDim(), left_->getMatrixValues());
	right_->devideIntoSubexpressions(subexpressions, matrixValuesCache);
}

std::shared_ptr<ExpAssign> ExpAssign::deepCopyAssignment() const{
	std::shared_ptr<ExpMatrix> lCopy = left_->deepCopyMatrix();
	std::shared_ptr<Expression> rCopy = right_->deepCopy();
	return std::make_shared<ExpAssign>(lCopy, rCopy, derType_, resultType_);
}

std::shared_ptr<Expression> ExpAssign::transformExpCholeskToExpLowUpp() const {
	std::shared_ptr<ExpMatrix> lCopy = left_->deepCopyMatrix();
	std::shared_ptr<Expression> rCopy = right_->transformExpCholeskToExpLowUpp();
	return std::make_shared<ExpAssign>(lCopy, rCopy, derType_, resultType_);
}

std::shared_ptr<ExpAssign> ExpAssign::startTransformExpCholeskToExpLowUpp() const {
	std::shared_ptr<ExpMatrix> lCopy = left_->deepCopyMatrix();
	std::shared_ptr<Expression> rCopy = right_->transformExpCholeskToExpLowUpp();
	return std::make_shared<ExpAssign>(lCopy, rCopy, derType_, resultType_);
}
