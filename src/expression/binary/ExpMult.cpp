#include "expression/binary/ExpMult.hpp"
#include <memory>
#include <string>
#include <vector>

#include "expression/Expression.hpp"
#include "expression/unary/ExpTrans.hpp"
#include "expression/binary/ExpAdd.hpp"
#include "expression/unary/ExpParenth.hpp"
#include "expression/ExpressionResultType.hpp"
#include "expression/ExpressionType.hpp"
#include "dimension/DimGraph.hpp"
#include "values/MatrixCache.hpp"
#include "exception/EigenADException.hpp"
#include <wx/msgdlg.h>

std::string ExpMult::toString() const {
	return "(" + left_->toString() + " * " + right_->toString() + ")";
}

std::shared_ptr<Expression> ExpMult::tangDifferentiate() const{
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		//both children return matrix values -> d(A*B) = dA * B  +  dB * A

		std::shared_ptr<Expression> copyleft = left_->deepCopy();
		std::shared_ptr<Expression> copyright = right_->deepCopy();

		std::shared_ptr<ExpMult> lMult = std::make_shared<ExpMult>(left_->tangDifferentiate(), copyright, ExpressionResultType::Matrix);

		std::shared_ptr<ExpMult> rMult = std::make_shared<ExpMult>(copyleft, right_->tangDifferentiate(), ExpressionResultType::Matrix);

		return std::make_shared<ExpAdd>(lMult, rMult, ExpressionResultType::Matrix);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Matrix) {
		//only multipliright returns a matrix value, only propagate there
		return std::make_shared<ExpMult>(left_->deepCopy(), right_->tangDifferentiate(), ExpressionResultType::Matrix);
	}
	else if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Scalar) {
		std::shared_ptr<ExpMult> mult = std::make_shared<ExpMult>(left_->tangDifferentiate(), right_->deepCopy(), ExpressionResultType::Matrix);
		return mult;
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		return deepCopy();
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "tangDifferentiate()");
	}
	//only Scalar return types -> no further matrices -> no further propagation
	
}

void ExpMult::adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		//both children return matrix values, forward function to both, copy tempAdj
		//because semanticCheck() already ran, initialize with respective ExpressionResultType
		std::shared_ptr<ExpTrans> transright = std::make_shared<ExpTrans>(right_->deepCopy(), resultType_);
		std::shared_ptr<ExpMult> tempAdjleft = std::make_shared<ExpMult>(tempAdj, transright, resultType_);

		left_->adjDifferentiate(tempAdjleft, allAdjoints);

		std::shared_ptr<ExpTrans> transleft = std::make_shared<ExpTrans>(left_->deepCopy(), ExpressionResultType::Matrix);
		std::shared_ptr<ExpMult> tempAdjright = std::make_shared<ExpMult>(transleft, tempAdj->deepCopy(), resultType_);
		
		right_->adjDifferentiate(tempAdjright, allAdjoints);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Matrix) {
		//only multipliright returns a matrix value, forward function only there, do not copy tempAdj
		std::shared_ptr<ExpMult> tempAdjright = std::make_shared<ExpMult>(left_->deepCopy(), tempAdj, resultType_);
		right_->adjDifferentiate(tempAdjright, allAdjoints);
	}
	else if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Scalar) {
		//only multileft returns a matrix value, forward function only there, do not copy tempAdj
		std::shared_ptr<ExpMult> tempAdjleft = std::make_shared<ExpMult>(tempAdj, right_->deepCopy(), resultType_);
		left_->adjDifferentiate(tempAdjleft, allAdjoints);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		//only Scalar return types -> no further matrices -> no further propagation
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "adjDifferentiate(...)");
	}
}

void ExpMult::setDimensionNodes(DimGraph& graph) {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		
		graph.connectToAndAddIfAbsent(left_->createRowDimNode(), getHashID(), DimensionType::Row);
		graph.connectToAndAddIfAbsent(right_->createColDimNode(), getHashID(), DimensionType::Column);

		graph.addIfAbsentAndConnect(left_->createColDimNode(), right_->createRowDimNode());

		left_->setDimensionNodes(graph);
		right_->setDimensionNodes(graph);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Matrix) {
		graph.connectToAndAddIfAbsent(right_->createRowDimNode(), getHashID(), DimensionType::Row);
		graph.connectToAndAddIfAbsent(right_->createColDimNode(), getHashID(), DimensionType::Column);

		right_->setDimensionNodes(graph);

	}
	else if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Scalar) {

		graph.connectToAndAddIfAbsent(left_->createRowDimNode(), getHashID(), DimensionType::Row);
		graph.connectToAndAddIfAbsent(left_->createColDimNode(), getHashID(), DimensionType::Column);

		left_->setDimensionNodes(graph);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		//only scalars: no further propagation
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "setDimensionNodes(...)");
	}
}

std::variant<ScalarValueType, MatrixType> ExpMult::calculate() const {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	std::variant<ScalarValueType, MatrixType> res;
	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		res = std::get<MatrixType>(left_->calculate()) * std::get<MatrixType>(right_->calculate());
		return res;
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Matrix) {
		res = std::get<ScalarValueType>(left_->calculate()) * std::get<MatrixType>(right_->calculate());
		return res;
	}
	else if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Scalar) {
		res = std::get<MatrixType>(left_->calculate()) * std::get<ScalarValueType>(right_->calculate());
		return res;
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		res = std::get<ScalarValueType>(left_->calculate()) * std::get<ScalarValueType>(right_->calculate());
		return res;
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "calculate()");
	}
}

std::shared_ptr<Expression> ExpMult::deepCopy() const {
	std::shared_ptr<Expression> lCopy = left_->deepCopy();
	std::shared_ptr<Expression> rCopy = right_->deepCopy();
	return std::make_shared<ExpMult>(lCopy, rCopy, resultType_);
}

ExpressionResultType ExpMult::checkSemantics() {
	//temporary variables such that on each child the checkSemantics() function gets called
	ExpressionResultType leftRT = left_->checkSemantics();
	ExpressionResultType rightRT = right_->checkSemantics();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		return resultType_ = ExpressionResultType::Matrix;
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Matrix) {
		return resultType_ = ExpressionResultType::Matrix;
	}
	else if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Scalar) {
		return resultType_ = ExpressionResultType::Matrix;;
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		return resultType_ = ExpressionResultType::Scalar;
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "checkSemantics()");
	}
}

std::vector<std::vector<ExpressionResultType>> ExpMult::getRequiredTypes() const {
	return { {ExpressionResultType::Matrix, ExpressionResultType::Matrix}, {ExpressionResultType::Matrix, ExpressionResultType::Scalar},
		{ExpressionResultType::Scalar, ExpressionResultType::Matrix}, {ExpressionResultType::Scalar, ExpressionResultType::Scalar} };
}

ExpressionType ExpMult::getExpressionType() const {
	return ExpressionType::Multiplication;
}

void ExpMult::perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	std::variant<ScalarValueType, MatrixType> res;
	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		left_->perturbeMatrixValuesBy(amount, matrixValueCache);
		right_->perturbeMatrixValuesBy(amount, matrixValueCache);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Matrix) {
		right_->perturbeMatrixValuesBy(amount, matrixValueCache);
	}
	else if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Scalar) {
		left_->perturbeMatrixValuesBy(amount, matrixValueCache);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "calculate()");
	}
}

std::shared_ptr<Expression> ExpMult::transformExpCholeskToExpLowUpp() const {
	return std::make_shared<ExpMult>(left_->transformExpCholeskToExpLowUpp(), right_->transformExpCholeskToExpLowUpp(), resultType_);
}