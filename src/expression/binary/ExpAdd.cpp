#include "expression/binary/ExpAdd.hpp"
#include <memory>
#include <string>
#include <vector>

#include "expression/Expression.hpp"
#include "values/MatrixCache.hpp"
#include "expression/ExpressionType.hpp"
#include "expression/ExpressionResultType.hpp"

#include "exception/EigenADException.hpp"

#include "EigenNS.hpp"
#include "dimension/DimGraph.hpp"

std::string ExpAdd::toString() const {
	return "(" + left_->toString() + " + " + right_->toString() + ")";
}

std::shared_ptr<Expression> ExpAdd::tangDifferentiate() const{

	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {

		return std::make_shared<ExpAdd>(left_->tangDifferentiate(), right_->tangDifferentiate(), resultType_);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		//only Scalar return types -> no further matrices -> no further differentiation -> just return deep copies
		return deepCopy();
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "tangDifferentiate()");
	}
}

void ExpAdd::adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		// propagate function to both children, copy tempAdj
		left_->adjDifferentiate(tempAdj, allAdjoints);
		right_->adjDifferentiate(tempAdj->deepCopy(), allAdjoints);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		// only Scalar return types -> no further matrices -> no further propagation
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "adjDifferentiate(...)");
	}
}

void ExpAdd::setDimensionNodes(DimGraph& graph) {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();
	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {

		graph.connectToAndAddIfAbsent(left_->createRowDimNode(), getHashID(), DimensionType::Row);
		graph.connectToAndAddIfAbsent(left_->createColDimNode(), getHashID(), DimensionType::Column);

		graph.connectToAndAddIfAbsent(right_->createRowDimNode(), getHashID(), DimensionType::Row);
		graph.connectToAndAddIfAbsent(right_->createColDimNode(), getHashID(), DimensionType::Column);


		left_->setDimensionNodes(graph);
		right_->setDimensionNodes(graph);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		//only Scalar return types -> no further matrices -> no further propagation
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "setDimensionNodes(...)");
	}
}

std::variant<ScalarValueType, MatrixType>  ExpAdd::calculate() const {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();
	std::variant<ScalarValueType, MatrixType> res;
	if (left_->getExpressionResultType() == ExpressionResultType::Matrix && right_->getExpressionResultType() == ExpressionResultType::Matrix) {
		res = std::get<MatrixType>(left_->calculate()) + std::get<MatrixType>(right_->calculate());
		return res;
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		//Scalar
		res = std::get<ScalarValueType>(left_->calculate()) + std::get<ScalarValueType>(right_->calculate());
		return res;
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "calculate()");
	}
}

std::shared_ptr<Expression> ExpAdd::deepCopy() const {
	std::shared_ptr<Expression> lCopy = left_->deepCopy();
	std::shared_ptr<Expression> rCopy = right_->deepCopy();
	return std::make_shared<ExpAdd>(lCopy, rCopy, resultType_);
}

ExpressionResultType ExpAdd::checkSemantics() {
	ExpressionResultType leftRT = left_->checkSemantics();
	ExpressionResultType rightRT = right_->checkSemantics();

	if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		return resultType_ = ExpressionResultType::Scalar;
	}
	else if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		return resultType_ = ExpressionResultType::Matrix;
	}
	else {
		throw InvalidMatrixExpressionException(getExpressionType(), {leftRT, rightRT}, getRequiredTypes(), toString(), "checkSemantics()");
	}
}

std::vector<std::vector<ExpressionResultType>> ExpAdd::getRequiredTypes() const {
	return { {ExpressionResultType::Matrix, ExpressionResultType::Matrix}, {ExpressionResultType::Scalar, ExpressionResultType::Scalar} };
}

ExpressionType ExpAdd::getExpressionType() const {
	return ExpressionType::Addition;
}

void ExpAdd::perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();
	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		left_->perturbeMatrixValuesBy(amount, matrixValueCache);
		right_->perturbeMatrixValuesBy(amount, matrixValueCache);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "calculate()");
	}
}

std::shared_ptr<Expression> ExpAdd::transformExpCholeskToExpLowUpp() const {
	return std::make_shared<ExpAdd>(left_->transformExpCholeskToExpLowUpp(), right_->transformExpCholeskToExpLowUpp(), resultType_);
}
