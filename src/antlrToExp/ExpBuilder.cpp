#include "antlrToExp/ExpBuilder.hpp"
#include <memory>
#include <vector>
#include <any>
#include <iostream>
#include <regex>

#include "EigenNS.hpp"
#include "expression/DerivativeType.hpp"
#include "expression/ExpressionType.hpp"
#include "expression/Expression.hpp"
#include "expression/binary/ExpAdd.hpp"
#include "expression/ExpAssign.hpp"
#include "expression/terminal/ExpMatrix.hpp"
#include "expression/binary/ExpMult.hpp"
#include "expression/unary/ExpNeg.hpp"
#include "expression/unary/ExpParenth.hpp"
#include "expression/terminal/ExpScalar.hpp"
#include "expression/binary/ExpSub.hpp"
#include "expression/unary/ExpTrans.hpp"
#include "expression/binary/ExpCholesk.hpp"

std::pair<std::string, int> ExpBuilder::getHighestOrder() const {
	return highestOrder_;
}

std::string ExpBuilder::getAliasedName() const {
	return aliasedFullText_;
}

std::any ExpBuilder::visitMatrixID(EigenExpParser::MatrixIDContext* ctx) {
	std::string text = ctx->children[0]->getText();
	std::string name;
	std::string suffixes;

	bool isAliased = false;
	if (aliasedFullText_ == text) {
		isAliased = true;
	}

	std::regex suffixPattern("(.*?)((_[ta][0-9]+)+$)");
	std::smatch match;

	

	if (std::regex_match(text, match, suffixPattern)) {
		name = match[1];
		suffixes = match[2];
		
	}
	else {
		name = text;
	}

	//do not allow LLT_SUBST names of matrices, add underscores
	std::regex lltPattern("^" + LLT_SUBST + "\\d+$");
	if (std::regex_match(name, lltPattern)) {
		name = "_" + name;
	}

	//do not allow DIMENSION_PREFIX names of matrices, add underscores
	std::regex dimPattern("^" + DIMENSION_PREFIX + "\\d+$");
	if (std::regex_match(name, dimPattern)) {
		name = "_" + name;
	}

	std::regex lastSuffixPattern("_[ta]([0-9]+)$");
	std::smatch lastSuffixMatch;

	if (std::regex_search(suffixes, lastSuffixMatch, lastSuffixPattern)) {

		int num = std::stoi(lastSuffixMatch[1]);

		if (num > highestOrder_.second) {
			highestOrder_ = std::make_pair(text, num);
		}

	}



	std::shared_ptr<Expression> mat = std::make_shared<ExpMatrix>(name, suffixes, isAliased);
	return mat;
}


std::any ExpBuilder::visitScalarValueID(EigenExpParser::ScalarValueIDContext* ctx) {
	std::string value = ctx->children[0]->getText();
	std::replace(value.begin(), value.end(), ',', '.');
	std::shared_ptr<Expression> scal = std::make_shared<ExpScalar>(std::stod(value));
	return scal;
}

std::any ExpBuilder::visitExpScalarValue(EigenExpParser::ExpScalarValueContext* ctx) {
	return visit(ctx->children[0]);
}

std::any ExpBuilder::visitAssignments(EigenExpParser::AssignmentsContext* ctx) 
{

	std::vector<std::shared_ptr<ExpAssign>> assignments;
	for (auto child : ctx->assignment()) {
		std::shared_ptr<ExpAssign> exp = std::any_cast<std::shared_ptr<ExpAssign>>(visit(child));

		assignments.push_back(exp);
		break; //<- remove this line for parser support for multiple equations independent of each other
	}

	return assignments;
}

std::any ExpBuilder::visitExpAssign(EigenExpParser::ExpAssignContext* ctx)
{
	std::shared_ptr<ExpMatrix> leftMatrix = std::dynamic_pointer_cast<ExpMatrix>(std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[0])));

	aliasedFullText_ = leftMatrix->toString();

	std::shared_ptr<Expression> right = std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[2]));
	std::shared_ptr<ExpAssign> eq = std::make_shared<ExpAssign>(leftMatrix, right);
	return eq;
}


std::any ExpBuilder::visitExpParenth(EigenExpParser::ExpParenthContext* ctx)
{
	std::shared_ptr<Expression> exp = std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[1]));

	if (exp->getExpressionType() == ExpressionType::Parenthesis) {
		return exp;
	}

	std::shared_ptr<Expression> par = std::make_shared<ExpParenth>(exp);

	return par;
}

std::any ExpBuilder::visitExpMatrix(EigenExpParser::ExpMatrixContext* ctx)
{
	return visit(ctx->children[0]);
}

std::any ExpBuilder::visitExpAdd(EigenExpParser::ExpAddContext* ctx)
{

	std::shared_ptr<Expression> left = std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[0]));
	std::shared_ptr<Expression> right = std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[2]));
	std::shared_ptr<Expression> add = std::make_shared<ExpAdd>(left, right);

	return add;
}

std::any ExpBuilder::visitExpNeg(EigenExpParser::ExpNegContext* ctx) {
	std::shared_ptr<Expression> exp = std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[1]));
	std::shared_ptr<Expression> neg = std::make_shared<ExpNeg>(exp);
	return neg;
}

std::any ExpBuilder::visitExpPos(EigenExpParser::ExpPosContext* ctx) {
	return visit(ctx->children[1]);
}

std::any ExpBuilder::visitExpTrans(EigenExpParser::ExpTransContext* ctx)
{
	std::shared_ptr<Expression> exp = std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[0]));
	std::shared_ptr<Expression> trans = std::make_shared<ExpTrans>(exp);

	return trans;
}

std::any ExpBuilder::visitExpSub(EigenExpParser::ExpSubContext* ctx)
{
	std::shared_ptr<Expression> left = std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[0]));
	std::shared_ptr<Expression> right = std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[2]));
	std::shared_ptr<Expression> sub = std::make_shared<ExpSub>(left, right);


	return sub;
}

std::any ExpBuilder::visitExpMult(EigenExpParser::ExpMultContext* ctx)
{
	std::shared_ptr<Expression> left = std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[0]));
	std::shared_ptr<Expression> right = std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[2]));
	std::shared_ptr<Expression> mult = std::make_shared<ExpMult>(left, right);
	return mult;
}

std::any ExpBuilder::visitExpCholesk(EigenExpParser::ExpCholeskContext* ctx)
{
	std::shared_ptr<Expression> left = std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[0]));
	std::shared_ptr<Expression> right = std::any_cast<std::shared_ptr<Expression>>(visit(ctx->children[2]));
	std::shared_ptr<Expression> cholesk = std::make_shared<ExpCholesk>(left, right);
	return cholesk;
}
