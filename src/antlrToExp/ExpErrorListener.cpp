#include "antlrToExp/ExpErrorListener.hpp"
#include <string>
#include <vector>

void ExpErrorListener::syntaxError(Recognizer* recognizer, Token* offendingSymbol, size_t line,
	size_t charPositionInLine, const std::string& msg, std::exception_ptr e)
{
	
	std::string errMsg;
	if (offendingSymbol == nullptr) {
		
		errMsg = "Token Recognition Error (line: " + std::to_string(line) + ", position: " + std::to_string(charPositionInLine + 1) + ")";
		
	}
	else {
		if (offendingSymbol->getText() == "<EOF>") {
			errMsg = "Syntax Error (line: " + std::to_string(line) + ", unexpected end)";
		}
		else {
			errMsg = "Syntax Error (line: " + std::to_string(line) + ", position: " + std::to_string(charPositionInLine + 1) +
				", " + "'" + offendingSymbol->getText() + "')";
		}
		
	}

	addErrorMsg(errMsg);
	hasError_ = true;
	
}

void ExpErrorListener::reportAmbiguity(Parser* recognizer, const dfa::DFA& dfa, size_t startIndex, size_t stopIndex,
	bool exact, const antlrcpp::BitSet& ambigAlts, atn::ATNConfigSet* configs)
{
	addErrorMsg("Parsing Error");
	hasError_ = true;
}

void ExpErrorListener::reportAttemptingFullContext(Parser* recognizer, const dfa::DFA& dfa, size_t startIndex, size_t stopIndex,
	const antlrcpp::BitSet& conflictingAlts, atn::ATNConfigSet* configs)
{
	addErrorMsg("Parsing Error");
	hasError_ = true;
}

void ExpErrorListener::reportContextSensitivity(Parser* recognizer, const dfa::DFA& dfa, size_t startIndex, size_t stopIndex,
	size_t prediction, atn::ATNConfigSet* configs)
{
	addErrorMsg("Parsing Error");
	hasError_ = true;
}


bool ExpErrorListener::getHasError()
{
	return hasError_;
}

std::vector<std::string> ExpErrorListener::getErrorMsgs()
{
	return errors_;
}

void ExpErrorListener::addErrorMsg(const std::string& msg)
{
	errors_.push_back(msg);
}

